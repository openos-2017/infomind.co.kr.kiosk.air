package infomind.co.kr.kis;

import infomind.co.kr.net.NetworkUtil;
import infomind.co.kr.util.LogUtil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class NetworkChangeReceiver extends BroadcastReceiver {
	 
    @Override
    public void onReceive(final Context context, final Intent intent) {
 
        String status = NetworkUtil.getConnectivityStatusString(context);
 
        Toast.makeText(context, status, Toast.LENGTH_LONG).show();
        LogUtil.d("NetworkChangeReceiver", status);
        
        if(NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_NOT_CONNECTED){
        	
        }
    }
}
