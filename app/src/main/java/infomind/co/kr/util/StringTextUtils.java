package infomind.co.kr.util;

/**
 * Created by jwh0728 on 2017. 9. 20..
 */

public class StringTextUtils {
    /**
     * paddingLeft
     * @param src
     * @param totalLength
     * @param padChracter
     * @return
     */
    public static String paddingLeft(String src, int totalLength, char padChracter){
        String temp = src;
        for(int i = 0; i < totalLength - temp.length(); i++){
            src = padChracter + src;
        }
        return src;
    }
}
