package infomind.co.kr.util;

import android.util.Log;

import infomind.co.kr.kiosk.air.BuildConfig;

public class LogUtil {
	static public void d(String tag, String message){
		if(BuildConfig.DEBUG) Log.d(tag, message);
	}
	static public void i(String tag, String message){
		if(BuildConfig.DEBUG) Log.i(tag, message);
	}
	static public void e(String tag, String message){
		if(BuildConfig.DEBUG) Log.e(tag, message);
	}
}
