package infomind.co.kr.data;

/**
 * Created by jwh0728 on 15. 3. 11..
 */

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import infomind.co.kr.util.LogUtil;

/**
 * @author jwh0728
 */
public class DBCommonAndroid extends SQLiteOpenHelper {

    //The Android's default system path of your application database.
    /**
     * 데이터 베이스 파일 이름
     */
    private static String DB_NAME = "infomind.co.kr.kiosk.db";

    /**
     * 데이터 베이스 객체
     */
    private SQLiteDatabase myDataBase;

    /**
     * Application context
     */
    private final Context myContext;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     * @param context
     */
    public DBCommonAndroid(Context context) throws IOException{

        super(context, DB_NAME, null, 1);
        this.myContext = context;

        checkOrOpenDataBase();
    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it opened or created, false if it failed
     */
    private boolean checkOrOpenDataBase(){

        try{
            String db_path = Environment.getExternalStorageDirectory() + File.separator + DB_NAME;
            File dbFile = new File(db_path);
            if(!dbFile.exists()) {
                myDataBase = SQLiteDatabase.openOrCreateDatabase(db_path, null);

                // create table
                myDataBase.execSQL("CREATE TABLE RETURN_OPTION ( " +
                        "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "DATE_TIME DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                        "DATA TEXT " +
                        ")");
                
            }else{
                myDataBase = SQLiteDatabase.openOrCreateDatabase(db_path, null);
            }
        }catch(SQLiteException e){
            //database does't exist yet.
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public synchronized void close() {

        if(myDataBase != null)
            myDataBase.close();

        super.close();

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }

    /** Inserts data.
     */
    public void insertReturnResultData(String resultJSONDdta){
        String query = "insert into RETURN_OPTION (DATA) values ("
                + "'" + resultJSONDdta + "'"
                + "  )";
        LogUtil.d("INSERT", query);
        myDataBase.execSQL(query);
    }
    
}
