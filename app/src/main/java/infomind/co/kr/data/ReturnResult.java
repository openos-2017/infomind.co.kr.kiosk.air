package infomind.co.kr.data;

import org.apache.http.util.ByteArrayBuffer;

/**
 * @author jwh0728 결과 처리용
 */
public class ReturnResult {
    public ReturnResult(boolean success, String message, ByteArrayBuffer data) {
        super();
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public ReturnResult(boolean success, String message) {
        super();
        this.success = success;
        this.message = message;
        this.data = null;
    }

    public ByteArrayBuffer getData() {
        return data;
    }

    public void setData(ByteArrayBuffer data) {
        this.data = data;
    }

    private ByteArrayBuffer data;
    private boolean success = false;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message = "";

    public String toJSONString(){
        return "success : " + isSuccess() + " message : " + getMessage() + " data : " + (data != null ? data.toString() : "");
    }

}