package infomind.co.kr.kicc;

import infomind.co.kr.data.ReturnResult;
import infomind.co.kr.net.MultiSerial;
import infomind.co.kr.util.LogUtil;

import android.content.Context;
import android.os.Handler;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Date;

import static infomind.co.kr.net.MultiSerial.CARD_READER_INDEX;
import static infomind.co.kr.net.MultiSerial.RECEIPT_PRINTER_INDEX;
import static infomind.co.kr.net.MultiSerial.TICKET_PRINTER_INDEX;


public class CardReader extends CardReaderCommon {

	/**
	 * 접속 체크시 접속 완료
	 */
	public static final int DEVICE_CONNECTED = 0;

	/**
	 * 접속 오류 : 권한 없음
	 */
	public static final int CANNOT_OPEN_NO_PERMISSION = 1;

	/**
	 * 접속 오류 : 장비의 칩이 다름
	 */
	public static final int CANNOT_OPEN_CHIP_NOT_SUPPORT = 2;

	/**
	 * 전송됨
	 */
	public static final int COMMAND_SEND_OK = 3;

	/**
	 * 전송 후 응답 대기 상태 (카드결제의 경우 단말기에 '카드를 읽혀주세요' 상태)
	 */
	public static final int ACK_READ = 4;

	/**
	 * 전송 후 결과 받음
	 */
	public static final int RESULT_RECEIVED = 5;

	/**
	 * 1차 재전송 시도
	 */
	public static final int RETRY_FIRST_SEND = 6;

	/**
	 * 1차 재전송 후 결과 반환
	 */
	public static final int RETRY_FIRST_RESULT_RECEIVED = 7;

	/**
	 * 2차 재전송 시도
	 */
	public static final int RETRY_SECOND_SEND = 8;

	/**
	 * 2차 재전송 결과 반환
	 */
	public static final int RETRY_SECOND_RESULT_RECEIVED = 9;

	/**
	 * 2차 전송 후 최종 에러
	 */
	public static final int RETRY_SECOND_ERROR = 10;

	/**
	 * 전문 오류
	 */
	public static final int COMMAND_ERROR = 11;

    public static final int NO_DEVICES = 12;

	public static final int FIND_CARD_DEVICE_END = 13;




	private OnReaderResultReceived onResultReceived;

	private Thread communicationThread = null;


	private String logoImagePath = "";

	private String LOG_TAG = "KICC";


	/**
	 * 생성자
	 */
	public CardReader(final Context context, MultiSerial multiSerial) {
		super(multiSerial);
	}

	/**
	 * POS MSR 정보 요청
	 */
    public void getReaderPosMSRInfo(final String parameter) {
        getReaderPosMSRInfo(parameter, CARD_READER_INDEX);
    }
	public void getReaderPosMSRInfo(final String parameter, final int deviceIndex) {
        ReaderRequestOptions requestOptions = new ReaderRequestOptions();
        requestOptions.setCMD(ReaderRequestOptions.ADDITIONAL_CODE);
        requestOptions.setGROUP(ReaderRequestOptions.GROUP_CODE);
        requestOptions.setTYPE(ReaderRequestOptions.F_CODE_DEVICE_INFO);

        final ReaderRequestOptions readerRequestOptions = requestOptions;

		if (communicationThread != null) {
			communicationThread.interrupt();
			try {
				while (communicationThread != null && communicationThread.isAlive()) {
					Thread.sleep(500);
				}
				communicationThread = null;
				LogUtil.d("getReaderPosMSRInfo", "thread set to null");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		communicationThread = new Thread(new Runnable() {

			@Override
			public void run() {
				getMultiSerial().openUARTDevice(deviceIndex);

				byte[] command;
				ReturnResult result;
				try {
					command = createReaderRequest(readerRequestOptions);
					result = doReaderRequest(deviceIndex, command);
					LogUtil.d("getReaderPosMSRInfo", result.getMessage());
					if (result.isSuccess()) {
						ReaderResultOptions resultOptions = parseReaderResponse(result.getData().toByteArray());
						onResultReceived.returnReceivedData(resultOptions, parameter);
					} else {
						onResultReceived.returnReceivedData(null, parameter);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		communicationThread.start();
	}

    /**
     * card data
     */
    public void getReaderCardData(ReaderRequestOptions.RequestCardData requestCardData, final String parameter) {
        // 거래 구분자
        requestCardData.setTransactionSeperator((byte)'2');
        // 거래 일시
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date today = new Date();
        String todayString = sdf.format(today);
        requestCardData.setTransactionTime(todayString.getBytes());
        // 단말기ID
        requestCardData.setTerminalID(CardReaderCommon.TERMINAL_NO_HEX);
        // EMV PIN 설정
        requestCardData.setEmvPinProperty((byte)'2');
        // Card Input 설정
        requestCardData.setCardInputProperty((byte)'1');
        // EMV MS 거래 허용여부 및 기타
        requestCardData.setEmvMSpermit((byte)'2');
        // 랜덤값
        requestCardData.setRandomValue("1234567890123456".getBytes());//(todayString + "01").getBytes());

        // 요청 정보
        ReaderRequestOptions requestOptions = new ReaderRequestOptions();

		// 명령/구분/기능 코드 설정
		requestOptions.setCMD((byte)ADDITIONAL_CODE);
		requestOptions.setGROUP((byte)GROUP_CODE);
		requestOptions.setTYPE((byte) F_CODE_CARD_DATA);
        // 요청 정보 SET
        requestOptions.setDATA(requestCardData.toByteArray());

		final ReaderRequestOptions cardDataRequestOptions = requestOptions;
        if (communicationThread != null) {
            communicationThread.interrupt();
            try {
                while (communicationThread != null && communicationThread.isAlive()) {
                    Thread.sleep(500);
                }
                communicationThread = null;
                LogUtil.d("getReaderPosMSRInfo", "thread set to null");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        communicationThread = new Thread(new Runnable() {

            @Override
            public void run() {
                getMultiSerial().openUARTDevice(CARD_READER_INDEX);

                byte[] command;
                ReturnResult result;
                try {
					//result = readerCardDetect(MultiSerial.CARD_READER_INDEX);
                    //LogUtil.d("readerCardDetect", result.getMessage());
                    //if(result.isSuccess()) {
                        command = createReaderRequest(cardDataRequestOptions);
                        result = doReaderRequest(CARD_READER_INDEX, command);
                        LogUtil.d("getReaderCardData", result.getMessage());
                        if (result.isSuccess()) {
                            ReaderResultOptions resultOptions = parseReaderResponse(result.getData().toByteArray());

                            onResultReceived.returnReceivedData(resultOptions, parameter);
                        } else {
                            onResultReceived.returnReceivedData(null, parameter);
                        }
                    //}
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        communicationThread.start();
    }

	/**
	 * 결과 수신 메소드
	 * 
	 * @return
	 */
	public OnReaderResultReceived getOnResultReceived() {
		return onResultReceived;
	}

	/**
	 * 결과 수신 메소드
	 * 
	 * @param onResultReceived
	 */
	public void setOnReaderResultReceived(OnReaderResultReceived onResultReceived) {
		this.onResultReceived = onResultReceived;
	}

	public void findCardDevices(){
		final int nCountDevices = getMultiSerial().getSerial().PL2303getProlificUSBDeviceCount();

        // check device count
        if(nCountDevices <= 0) {
            getHandler().sendEmptyMessage(NO_DEVICES);
            return;
        }

        setOnReaderResultReceived(new OnReaderResultReceived() {
            @Override
            public void returnReceivedData(ReaderResultOptions resultOptions, String parameter) throws JSONException {
                int index = Integer.valueOf(parameter);

                ReaderResultOptions.PosMsrInfo posMsrInfo = resultOptions.getPosMsrInfo();
                if(posMsrInfo.getSerialCode().length > 0){
                    // find card reader
                    LogUtil.e("Serial", "POS MSR Info Serial : " + new String(posMsrInfo.getSerialCode()) + ", index : " + index);
                    // set CARD_READER_INDEX
                    CARD_READER_INDEX = index;
                    if(index == 0){
                        TICKET_PRINTER_INDEX = 1;
                        RECEIPT_PRINTER_INDEX = 2;
                    }else if(index == 1){
                        TICKET_PRINTER_INDEX = 0;
                        RECEIPT_PRINTER_INDEX = 2;
                    }else if(index == 2){
                        TICKET_PRINTER_INDEX = 0;
                        RECEIPT_PRINTER_INDEX = 1;
                    }

					getHandler().sendEmptyMessage(FIND_CARD_DEVICE_END);
                    return;
                }

            }
        });

        for(int i = 0; i < nCountDevices; i++) {
            LogUtil.d("Serial", "Try get POS MSR : " + i);

            getMultiSerial().openUARTDevice(i, getMultiSerial().getgUARTInfoList()[CARD_READER_INDEX]);
            getReaderPosMSRInfo(String.valueOf(i), i);

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

}
