package infomind.co.kr.kicc;


public class ReaderResultOptions {
    // 응답 코드
    private byte code;

    // 화면 표시 데이터
    private byte[] errorData;

    // 카드 구분자
    private byte[] cardType = new byte[4];

    // CVM
    private byte cvm;

    // 온라인 여부
    private byte isOnline;

    // 카드 정보
    private CardInfo cardInfo;

    // Chip Data 정보
    private byte[] chipData;

    // Online PIN Block
    private byte[] onlinePinBlock;

    // Sign Block
    private byte[] signBlock;

    // 멤버쉽 정보
    private byte[] membership;

    public byte getCode() {
        return code;
    }

    public void setCode(byte code) {
        this.code = code;
    }

    public byte[] getErrorData() {
        return errorData;
    }

    public void setErrorData(byte[] errorData) {
        this.errorData = errorData;
    }

    public byte[] getCardType() {
        return cardType;
    }

    public void setCardType(byte[] cardType) {
        this.cardType = cardType;
    }

    public byte getCvm() {
        return cvm;
    }

    public void setCvm(byte cvm) {
        this.cvm = cvm;
    }

    public byte getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(byte isOnline) {
        this.isOnline = isOnline;
    }

    public CardInfo getCardInfo() {
        return cardInfo;
    }

    public void setCardInfo(CardInfo cardInfo) {
        this.cardInfo = cardInfo;
    }

    public byte[] getChipData() {
        return chipData;
    }

    public void setChipData(byte[] chipData) {
        this.chipData = chipData;
    }


    /**
     * 카드 정보 객체
     */
    public static class CardInfo {
        private int lenCardInfo;

        private int lenTrack;

        private byte [] trackData;

        private byte [] modelCode = new byte[4];

        private byte [] version = new byte[4];

        private byte [] serial = new byte[12];

        private byte [] protocolVersion = new byte[2];

        private ENCData encData;

        public int getLenCardInfo() {
            return lenCardInfo;
        }

        public void setLenCardInfo(int lenCardInfo) {
            this.lenCardInfo = lenCardInfo;
        }

        public int getLenTrack() {
            return lenTrack;
        }

        public void setLenTrack(int lenTrack) {
            this.lenTrack = lenTrack;
        }

        public byte[] getModelCode() {
            return modelCode;
        }

        public void setModelCode(byte[] modelCode) {
            this.modelCode = modelCode;
        }

        public byte[] getVersion() {
            return version;
        }

        public void setVersion(byte[] version) {
            this.version = version;
        }

        public byte[] getSerial() {
            return serial;
        }

        public void setSerial(byte[] serial) {
            this.serial = serial;
        }

        public byte[] getProtocolVersion() {
            return protocolVersion;
        }

        public void setProtocolVersion(byte[] protocolVersion) {
            this.protocolVersion = protocolVersion;
        }

        public byte[] getTrackData() {
            return trackData;
        }

        public void setTrackData(byte[] trackData) {
            this.trackData = trackData;
        }

        public ENCData getEncData() {
            return encData;
        }

        public void setEncData(ENCData encData) {
            this.encData = encData;
        }

        /**
         * ENC Data를 가진 객체
         */
        public static class ENCData {
            private byte vanCount;

            private int vanEncDataLen;

            private byte[] vanCode;

            private byte[] version = new byte[4];

            private byte[] encType = new byte[2];

            private int dataLen;

            private String ksn;

            private byte[] encData;

            private byte hashtype;

            private byte hash;

            public byte getVanCount() {
                return vanCount;
            }

            public void setVanCount(byte vanCount) {
                this.vanCount = vanCount;
            }

            public int getVanEncDataLen() {
                return vanEncDataLen;
            }

            public void setVanEncDataLen(int vanEncDataLen) {
                this.vanEncDataLen = vanEncDataLen;
            }

            public byte[] getVanCode() {
                return vanCode;
            }

            public void setVanCode(byte[] vanCode) {
                this.vanCode = vanCode;
            }

            public byte[] getVersion() {
                return version;
            }

            public void setVersion(byte[] version) {
                this.version = version;
            }

            public byte[] getEncType() {
                return encType;
            }

            public void setEncType(byte[] encType) {
                this.encType = encType;
            }

            public int getDataLen() {
                return dataLen;
            }

            public void setDataLen(int dataLen) {
                this.dataLen = dataLen;
            }

            public String getKsn() {
                return ksn;
            }

            public void setKsn(String ksn) {
                this.ksn = ksn;
            }

            public byte[] getEncData() {
                return encData;
            }

            public void setEncData(byte[] encData) {
                this.encData = encData;
            }

            public byte getHashtype() {
                return hashtype;
            }

            public void setHashtype(byte hashtype) {
                this.hashtype = hashtype;
            }

            public byte getHash() {
                return hash;
            }

            public void setHash(byte hash) {
                this.hash = hash;
            }
        }
    }

    private PosMsrInfo posMsrInfo;

    public PosMsrInfo getPosMsrInfo() {
        return posMsrInfo;
    }

    public void setPosMsrInfo(PosMsrInfo posMsrInfo) {
        this.posMsrInfo = posMsrInfo;
    }

    public static class PosMsrInfo{
        private byte responseCode;

        private byte [] modelCode;

        private byte [] version;

        private byte [] serialCode;

        private byte [] protocolVersion;

        private byte receiveTrack;

        private byte [] passwordKey;

        private byte [] securityCode;

        public byte getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(byte responseCode) {
            this.responseCode = responseCode;
        }

        public byte[] getModelCode() {
            return modelCode;
        }

        public void setModelCode(byte[] modelCode) {
            this.modelCode = modelCode;
        }

        public byte[] getVersion() {
            return version;
        }

        public void setVersion(byte[] version) {
            this.version = version;
        }

        public byte[] getSerialCode() {
            return serialCode;
        }

        public void setSerialCode(byte[] serialCode) {
            this.serialCode = serialCode;
        }

        public byte[] getProtocolVersion() {
            return protocolVersion;
        }

        public void setProtocolVersion(byte[] protocolVersion) {
            this.protocolVersion = protocolVersion;
        }

        public byte getReceiveTrack() {
            return receiveTrack;
        }

        public void setReceiveTrack(byte receiveTrack) {
            this.receiveTrack = receiveTrack;
        }

        public byte[] getPasswordKey() {
            return passwordKey;
        }

        public void setPasswordKey(byte[] passwordKey) {
            this.passwordKey = passwordKey;
        }

        public byte[] getSecurityCode() {
            return securityCode;
        }

        public void setSecurityCode(byte[] securityCode) {
            this.securityCode = securityCode;
        }
    }
}
