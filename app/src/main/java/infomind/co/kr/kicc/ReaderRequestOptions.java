package infomind.co.kr.kicc;

import org.apache.http.util.ByteArrayBuffer;

/**
 * 송신 내용
 */
public class ReaderRequestOptions {

    // ServiceCode
    public static final byte ADDITIONAL_CODE	= (byte) 0xFB; // 확장코드
    public static final byte GROUP_CODE			= 0x11; // 그룹코드
    public static final byte F_CODE_DEVICE_INFO	= 0x02; // 기능(업무)코드 : POS MSR 단말정보 요청 (버전, 알고리즘)
    public static final byte F_CODE_CARD_DETECT	= 0x09; // 기능(업무)코드 : IC Card Detect
    public static final byte F_CODE_EMV_COMPLETE = 0x18; // 기능(업무)코드 : IC EMV 완료요청
    public static final byte F_CODE_CARD_INFO	= 0x20; // 기능(업무)코드 : IC 카드 데이터 요청

    private byte STX = 0x02;

    private byte [] LEN = new byte[2];

    private byte CNT;

    private byte CMD;

    private byte GROUP;

    private byte TYPE;

    private byte [] DATA = null;

    private byte ETX = 0x03;

    private byte [] CRC = new byte[2];

    public byte getSTX() {
        return STX;
    }

    public void setSTX(byte STX) {
        this.STX = STX;
    }

    public byte[] getLEN() {
        return LEN;
    }

    public void setLEN(byte[] LEN) {
        this.LEN = LEN;
    }

    public byte getCNT() {
        return CNT;
    }

    public void setCNT(byte CNT) {
        this.CNT = CNT;
    }

    public byte getCMD() {
        return CMD;
    }

    public void setCMD(byte CMD) {
        this.CMD = CMD;
    }

    public byte getGROUP() {
        return GROUP;
    }

    public void setGROUP(byte GROUP) {
        this.GROUP = GROUP;
    }

    public byte getTYPE() {
        return TYPE;
    }

    public void setTYPE(byte TYPE) {
        this.TYPE = TYPE;
    }

    public byte[] getDATA() {
        return DATA;
    }

    public void setDATA(byte[] DATA) {
        this.DATA = DATA;
    }

    public byte getETX() {
        return ETX;
    }

    public void setETX(byte ETX) {
        this.ETX = ETX;
    }

    public byte[] getCRC() {
        return CRC;
    }

    public void setCRC(byte[] CRC) {
        this.CRC = CRC;
    }

    /**
     * 카드 전송 데이터 클래스
     */
    public static class RequestCardData {
        // 거래 구분자
        private byte transactionSeperator;
        // 거래 종류
        private byte transactionType;
        // 거래 일시
        private byte [] transactionTime = new byte[14];
        // 거래금액
        private byte [] transactionAmount = new byte[9];

        private byte [] terminalID = new byte[8];
        // EMV PIN 설정
        private byte emvPinProperty;
        // 카드 입력 정보
        private byte cardInputProperty;
        // EMV MS 거래 허용여부 및 기타
        private byte emvMSpermit;
        // 랜덤값
        private byte [] randomValue = new byte[16];

        // byteArray로 리턴
        public byte [] toByteArray(){
            ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(47);
            byteArrayBuffer.append(transactionSeperator);
            byteArrayBuffer.append(transactionType);
            byteArrayBuffer.append(transactionTime, 0, 14);
            byteArrayBuffer.append(transactionAmount, 0, 9);
            byteArrayBuffer.append(terminalID, 0, 8);
            byteArrayBuffer.append(emvPinProperty);
            byteArrayBuffer.append(cardInputProperty);
            byteArrayBuffer.append(emvMSpermit);
            byteArrayBuffer.append(randomValue, 0, 16);
            return byteArrayBuffer.toByteArray();
        }

        public byte getTransactionSeperator() {
            return transactionSeperator;
        }

        public void setTransactionSeperator(byte transactionSeperator) {
            this.transactionSeperator = transactionSeperator;
        }

        public byte getTransactionType() {
            return transactionType;
        }

        public void setTransactionType(byte transactionType) {
            this.transactionType = transactionType;
        }

        public byte[] getTransactionTime() {
            return transactionTime;
        }

        public void setTransactionTime(byte[] transactionTime) {
            this.transactionTime = transactionTime;
        }

        public byte[] getTransactionAmount() {
            return transactionAmount;
        }

        public void setTransactionAmount(byte[] transactionAmount) {
            this.transactionAmount = transactionAmount;
        }

        public byte[] getTerminalID() {
            return terminalID;
        }

        public void setTerminalID(byte[] terminalID) {
            this.terminalID = terminalID;
        }

        public byte getEmvPinProperty() {
            return emvPinProperty;
        }

        public void setEmvPinProperty(byte emvPinProperty) {
            this.emvPinProperty = emvPinProperty;
        }

        public byte getCardInputProperty() {
            return cardInputProperty;
        }

        public void setCardInputProperty(byte cardInputProperty) {
            this.cardInputProperty = cardInputProperty;
        }

        public byte getEmvMSpermit() {
            return emvMSpermit;
        }

        public void setEmvMSpermit(byte emvMSpermit) {
            this.emvMSpermit = emvMSpermit;
        }

        public byte[] getRandomValue() {
            return randomValue;
        }

        public void setRandomValue(byte[] randomValue) {
            this.randomValue = randomValue;
        }
    }
}
