package infomind.co.kr.kicc;

import android.os.Handler;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import infomind.co.kr.data.ReturnResult;
import infomind.co.kr.net.MultiSerial;
import infomind.co.kr.util.LogUtil;

public class CardReaderCommon {

    public static final int STX				    = 0x02;
    public static final int ETX				    = 0x03;

    public static final int ACK				    = 0x06;
    public static final int NAK				    = 0x15;

    // ServiceCode
    public static final int ADDITIONAL_CODE	    = 0xFB; // 확장코드
    public static final int GROUP_CODE			= 0x11; // 그룹코드
    public static final int F_CODE_DEVICE_INFO	= 0x02; // 기능(업무)코드 : POS MSR 단말정보 요청 (버전, 알고리즘)
    public static final int F_CODE_CARD_DETECT	= 0x09; // 기능(업무)코드 : IC Card Detect
    public static final int F_CODE_EMV_COMPLETE = 0x18; // 기능(업무)코드 : IC EMV 완료요청
    public static final int F_CODE_CARD_DATA = 0x20; // 기능(업무)코드 : IC 카드 데이터 요청

    private String LOG_TAG = "CardReaderCommon";

    public static final int IC_FAIL_WAIT_MSR = 14;

    public static final byte [] TERMINAL_NO_HEX = new byte[]{
        0x30, 0x30, 0x37, 0x30, 0x30, 0x30, 0x38, 0x31
    };

    /**
     * 메시지 핸들러
     */
    private Handler handler = new Handler();

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    /**
     * Serial data
     */
    private MultiSerial multiSerial;

    public MultiSerial getMultiSerial() {
        return multiSerial;
    }

    public void setMultiSerial(MultiSerial multiSerial) {
        this.multiSerial = multiSerial;
    }

    /**
     * 패킷 카운터
     */
    private int packetCounter = 0;

    public CardReaderCommon(MultiSerial multiSerial){
        this.multiSerial = multiSerial;
    }

    /** 장비에 전송할 전문을 만든다
     * @return
     * @throws IOException
     */
    protected byte[] createReaderRequest(ReaderRequestOptions requestOptions) throws IOException {
        ByteArrayBuffer request = new ByteArrayBuffer(4096);

        /* STX 뒤에서 더함 */

		/* CNT : 패킷 카운터 (1~255) 송수신마다 1씩 증가. 255넘으면 1로 리 */
        if(++packetCounter > 255){
            packetCounter = 1;
        }
        request.append(packetCounter);

		/* CMD : 업무코드  */
        request.append(requestOptions.getCMD());
        request.append(requestOptions.getGROUP());
        request.append(requestOptions.getTYPE());

		/* DATA : 전문내용 */
		byte[] data = requestOptions.getDATA();
        if(data != null && data.length > 0) {
            request.append(data, 0, data.length);
        }

        ByteArrayBuffer lengthRequest = new ByteArrayBuffer(4096);
		/* LEN : 전송 패킷 길이 (LEN to DATA)*/
        byte [] length = intToByteArray(request.length() + 2);

		/* LEN 2bytes*/
        byte [] length2 = {length[2], length[3]}; // 2자리 자름
        lengthRequest.append(length2, 0, length2.length);

		/* ETX */
        request.append(requestOptions.getETX());

		/* LEN ~ EXT */
        lengthRequest.append(request.toByteArray(), 0, request.length());

		/* CRC삽입 */
        byte [] checksum = checkSum(lengthRequest.toByteArray());
        byte [] checksum2 = {checksum[2], checksum[3]};

        ByteArrayBuffer totalRequest = new ByteArrayBuffer(4096);
		/* STX */
        totalRequest.append(requestOptions.getSTX());

		/* LEN ~ CRC */
        totalRequest.append(lengthRequest.toByteArray(), 0, lengthRequest.length());
        totalRequest.append(checksum2, 0, checksum2.length);

        byte [] returnValue = totalRequest.toByteArray();

        return returnValue;
    }

    /**
     * Card를 꼽았을 때 수신, 결과를 리더기에 ACK or NAK를 송신한다.
     * @param deviceIndex
     * @return
     * @throws Exception
     */
    protected ReturnResult readerCardDetect(int deviceIndex) throws Exception {
        int readLen1;
        byte[] readBuf1 = new byte[4096];

        boolean success = false;
        String message = "";

        ByteArrayBuffer buffer = new ByteArrayBuffer(4096);

        do {
            while ((readLen1 = multiSerial.getSerial().PL2303Read(MultiSerial.CARD_READER_INDEX, readBuf1)) <= 0) {
                Thread.yield();
                //				Thread.sleep(500);
                if (Thread.currentThread().isInterrupted()) {
                    return new ReturnResult(false, "interrupted!!");
                }
            }
            buffer.append(readBuf1, 0, readLen1);

            if (buffer.length() >= 3) {
                int index = 0;
                // response check
                if (buffer.byteAt(index++) == STX) {
                    int length = (buffer.byteAt(index++) & 0xff00) | (buffer.byteAt(index++) & 0x00ff);
                    if(length > buffer.length() - 3 - 1 - 1 - 2) { // total length - ack - stx - etx - crc
                        // go receiving
                        continue;
                    }
                    byte cnt = (byte) buffer.byteAt(index++);
                    byte cmd = (byte) buffer.byteAt(index++);
                    byte group = (byte) buffer.byteAt(index++);
                    byte type = (byte) buffer.byteAt(index++);

                    byte responseCode = (byte) buffer.byteAt(index++);
                    byte[] inputFlag = new byte[]{(byte) buffer.byteAt(index++), (byte) buffer.byteAt(index++)};

                    byte[] command = null;

                    if (responseCode == 0x00) {
                        success = true;
                        command = new byte[]{ACK, ACK, ACK};
                        message = "received success";
                    } else if (responseCode == (byte) 0xFF) {
                        success = false;
                        command = new byte[]{NAK, NAK, NAK};
                        message = "received fail";
                    }

                    if (command != null) multiSerial.getSerial().PL2303Write(deviceIndex, command);

                    break;
                }
            }
        }while(true);

        LogUtil.d(LOG_TAG, buffer.toString());

        return new ReturnResult(success, message, buffer);
    }

    /**
     * POS MSR 정보를 요청
     *
     * @param command
     * @return
     * @throws Exception
     */
    protected ReturnResult doReaderRequest(int deviceIndex, byte[] command) throws Exception {

        if( command==null ) { //str is empty
            LogUtil.d(LOG_TAG , "WriteToUARTDevice: no data to write");
            return new ReturnResult(false, "WriteToUARTDevice: no data to write");
        }

        int res = multiSerial.getSerial().PL2303Write(deviceIndex, command);
        if( res < 0 ) {
            LogUtil.d(LOG_TAG, "w: fail to write: "+ res);
            return new ReturnResult(false, "w: fail to write: "+ res);
        }

        int readLen1;

        boolean success = false;
        ByteArrayBuffer buffer = new ByteArrayBuffer(4096);
        int index = 0;
        do{
            byte[] readBuf1 = new byte[4096];
            while((readLen1 = multiSerial.getSerial().PL2303Read(deviceIndex, readBuf1)) <= 0){
                Thread.yield();
//				Thread.sleep(500);
                if(Thread.currentThread().isInterrupted()){
                    return new ReturnResult(false, "interrupted!!");
                }
            }
            buffer.append(readBuf1, 0, readLen1);
            LogUtil.d("received", "buffer length : " + buffer.length());

            if(buffer.length() >= 3){

                // ACK check
                if(buffer.byteAt(index) == ACK && buffer.byteAt(index + 1) == ACK && buffer.byteAt(index + 2) == ACK) {
                    // index increase
                    index += 3;
                }else if(buffer.byteAt(index) == NAK && buffer.byteAt(index + 1) == NAK && buffer.byteAt(index + 2) == NAK){
                    // NAK
                    return new ReturnResult(false, "Recevied NAK");
                }

                // response check
                if(buffer.length() >= index + 7){
                    LogUtil.d("received", "buffer length : " + buffer.length() + " first : " + buffer.byteAt(index) + " index : " + index);

                    // find command
                    if(buffer.byteAt(index) == (byte)STX && buffer.byteAt(index + 4) == (byte)ADDITIONAL_CODE &&
                            buffer.byteAt(index + 5) == (byte)GROUP_CODE && buffer.byteAt(index + 6) == (byte)F_CODE_CARD_DATA){
                        // get length
                        int length = bytesToInt(new byte[]{(byte)buffer.byteAt(index + 1), (byte)buffer.byteAt(index + 2)});
                        LogUtil.d("received", "buffer length : " + buffer.length() + " len : " + length);

                        // check length
                        if(length > buffer.length() - index - 1 - 1 - 2){  // buffer_length - index - stx - etx - crc
                            // receive again
                            continue;
                        }else {
                            // IC CARD DATA
                            if(buffer.byteAt(index + 7) == 0x00){
                                // if result code == 0x00 then send ack and receive again
                                // send ack
                                command = new byte[]{ACK, ACK, ACK};
                                multiSerial.getSerial().PL2303Write(deviceIndex, command);
                                if( res < 0 ) {
                                    LogUtil.d(LOG_TAG, "w: fail to write: "+ res);
                                    return new ReturnResult(false, "w: fail to write: "+ res);
                                }
                                LogUtil.d("received", "buffer length : " + buffer.length() + " receive : 0x00");

                                // index increase
                                index += length + 1 + 2 + 1; // index + length + etx + crc + next

                                // receive another data
                                continue;
                            }else if(buffer.byteAt(index + 7) == 0x02) {
                                // receive complete send ack and return result
                                LogUtil.d("received", "buffer length : " + buffer.length() + " receive : 0x02");

                                // send ack
                                command = new byte[]{ACK, ACK, ACK};
                                multiSerial.getSerial().PL2303Write(deviceIndex, command);
                                if (res < 0) {
                                    LogUtil.d(LOG_TAG, "w: fail to write: " + res);
                                    return new ReturnResult(false, "w: fail to write: " + res);
                                }
                                ByteArrayBuffer arrayBuffer = new ByteArrayBuffer(1 + length + 1 + 2);
                                arrayBuffer.append(buffer.toByteArray(), index, 1 + length + 1 + 2);
                                return new ReturnResult(true, "received", arrayBuffer);
                            }else if(buffer.byteAt(index + 7) == (byte)0xf3){
                                //
                                LogUtil.d("received", "buffer length : " + buffer.length() + " receive : 0xf3");

                                // send ack
                                command = new byte[]{ACK, ACK, ACK};
                                multiSerial.getSerial().PL2303Write(deviceIndex, command);
                                if (res < 0) {
                                    LogUtil.d(LOG_TAG, "w: fail to write: " + res);
                                    return new ReturnResult(false, "w: fail to write: " + res);
                                }

                                getHandler().sendEmptyMessage(IC_FAIL_WAIT_MSR);

                                // index increase
                                index += length + 1 + 2 + 1; // index + length + etx + crc + next

                                // wait for receiving MSR
                                continue;
                            }else{
                                LogUtil.d("received", "buffer length : " + buffer.length() + " receive : " + buffer.byteAt(index + 7));
                            }
                        }
                    }else if(buffer.byteAt(index) == (byte)STX && buffer.byteAt(index + 4) == (byte)ADDITIONAL_CODE &&
                            buffer.byteAt(index + 5) == (byte)GROUP_CODE && buffer.byteAt(index + 6) == (byte)F_CODE_DEVICE_INFO){
                        // get length
                        int length = bytesToInt(new byte[]{(byte)buffer.byteAt(index + 1), (byte)buffer.byteAt(index + 2)});
                        LogUtil.d("received", "buffer length : " + buffer.length() + " len : " + length);

                        // check length
                        if(length > buffer.length() - index - 1 - 1 - 2){  // buffer_length - index - stx - etx - crc
                            // receive again
                            continue;
                        }
                    }else{
                        // when received card detect, etc, wait
                        // get length
                        int length = bytesToInt(new byte[]{(byte)buffer.byteAt(index + 1), (byte)buffer.byteAt(index + 2)});
                        LogUtil.d("received", "buffer length : " + buffer.length() + " len : " + length);

                        // check length
                        if(length > buffer.length() - index - 1 - 1 - 2){  // buffer_length - index - stx - etx - crc
                            // receive again
                            continue;
                        }

                        // index increase
                        index += length + 1 + 2 + 1; // index + length + etx + crc + next

                        // wait for receiving MSR
                        continue;
                    }

                    success = true;
                    break;
                }else{
                    continue;
                }

            }
        }while(!success);

        LogUtil.d(LOG_TAG, buffer.toString());

        // send ack
        command = new byte[]{ACK, ACK, ACK};
        multiSerial.getSerial().PL2303Write(deviceIndex, command);
        if( res < 0 ) {
            LogUtil.d(LOG_TAG, "w: fail to write: "+ res);
            return new ReturnResult(false, "w: fail to write: "+ res);
        }

        DelayTime(60);

        return new ReturnResult(true, "received", buffer);

    }

	/**
	 * int to byte array
	 * @param integer
	 * @return
	 */
	protected byte[] intToByteArray(final int integer) {
		ByteBuffer buff = ByteBuffer.allocate(Integer.SIZE / 8);
		buff.putInt(integer);
		buff.order(ByteOrder.BIG_ENDIAN);
		//buff.order(ByteOrder.LITTLE_ENDIAN);
		return buff.array();
	}
	
	/** checksum
	 * @param arr
	 * @return
	 */
	protected byte [] checkSum(byte[] arr){

		int temp;
		int SEED = 0x8005;
		int crc = 0xffff;
		int i = 0;
		int length = arr.length;
		
		do{
			temp = arr[i++] & 0xff;
			for(int j = 0; j < 8; j++){
				if(((crc & 0x0001) ^ (temp & 0x01)) == 1){ 	// 우측 첫번째 비트 비교해서 1로 다른지 
					crc = (crc >> 1) ^ SEED; 				// 우측으로 1쉬프트 후 SEED와 ^
				}else{
					crc >>= 1;								// 같으 우측 1 쉬프트
				}
				temp >>= 1;
			}
		}while( --length != 0);
		crc = ~crc & 0xffff;
		temp = crc;
		crc = (((crc << 8) & 0xffff) | ((temp >> 8) & 0xff) & 0xffff);
		
		return intToByteArray(crc);
	}
	

	/** 통신 응답전문 분석 
	 * @return
	 * @throws JSONException
	 */
	protected ReaderResultOptions parseReaderResponse(byte [] result) {
		// result save
		ReaderResultOptions resultOptions = new ReaderResultOptions();

		// length check
		if(result.length < 8) return null;

        int index = 0;

		// find STX
		while(result[index++] != STX && index <= 3) {
            continue;
        }

		// LEN
		int length = bytesToInt(new byte[]{result[index++], result[index++]});

		// CNT
		byte cnt = result[index++];

		// CMD
        byte additional_code = result[index++];

		if( additional_code == (byte)ADDITIONAL_CODE){
            byte group_code = result[index++];
			if(group_code == GROUP_CODE) {
                byte function_code = result[index++];
				if (function_code == F_CODE_DEVICE_INFO) {
					// POS MSR 정보 요청
					int dataLength = length - 1 - 1 - 1 - 1; // LEN - CNT- 0xFB - 0x11 - 0x02

					byte responseCode = result[index++];
					byte[] modelCode = {result[index++], result[index++], result[index++], result[index++]};
					byte[] version = {result[index++], result[index++], result[index++], result[index++]};
					byte[] serialCode = new byte[12];
					for (int i = 0; i < serialCode.length; i++) {
						serialCode[i] = result[index++];
					}
					byte[] protocolVersion = {result[index++], result[index++]};
					byte receiveTrack = result[index++];
					byte[] passwordKey = new byte[22];
					for (int i = 0; i < passwordKey.length; i++) {
						passwordKey[i] = result[index++];
					}
					byte[] securityCode = new byte[16];
					for (int i = 0; i < securityCode.length; i++) {
						securityCode[i] = result[index++];
					}

					ReaderResultOptions.PosMsrInfo posMsrInfo = new ReaderResultOptions.PosMsrInfo();
                    posMsrInfo.setResponseCode(responseCode);
                    posMsrInfo.setModelCode(modelCode);
                    posMsrInfo.setVersion(version);
                    posMsrInfo.setSerialCode(serialCode);
                    posMsrInfo.setProtocolVersion(protocolVersion);
                    posMsrInfo.setReceiveTrack(receiveTrack);
                    posMsrInfo.setPasswordKey(passwordKey);
                    posMsrInfo.setSecurityCode(securityCode);

                    resultOptions.setPosMsrInfo(posMsrInfo);

				} else if (function_code == F_CODE_CARD_DETECT) {
					// CARD DETECT
					byte responseCode = result[index++];
					byte[] inputFlag = {result[index++], result[index++]};

				} else if (function_code == F_CODE_CARD_DATA) {
					// CARD DATA
					resultOptions.setCode(result[index++]);
					if(resultOptions.getCode() >= 0xf0){
						// error
					}else{
						// success
						resultOptions.setCardType(new byte[]{
								result[index++],
								result[index++],
								result[index++],
								result[index++]
						});

						// cvm
						resultOptions.setCvm(result[index++]);

						// isOnline
						resultOptions.setIsOnline(result[index++]);

                        /////////////////////////////////////////////////////////////////////////
                        // card info
						ReaderResultOptions.CardInfo cardInfo = new ReaderResultOptions.CardInfo();

                        // card Info len
                        cardInfo.setLenCardInfo(Integer.valueOf(bytesToHex(new byte[]{result[index++], result[index++]})));

						// card Info track len
						cardInfo.setLenTrack(Integer.valueOf(bytesToHex(new byte[]{result[index++], result[index++]})));

						// card Infotrack data
						ByteArrayBuffer trackData = new ByteArrayBuffer(cardInfo.getLenTrack());
						trackData.append(result, index, cardInfo.getLenTrack());
						index += cardInfo.getLenTrack();
						cardInfo.setTrackData(trackData.toByteArray());

						// card Info model code
						cardInfo.setModelCode(new byte[]{
								result[index++],
								result[index++],
								result[index++],
								result[index++]
						});

						// card Info version
						cardInfo.setVersion(new byte[]{
								result[index++],
								result[index++],
								result[index++],
								result[index++]
						});

						// card Info serial
						cardInfo.setSerial(new byte[]{
								result[index++],
								result[index++],
								result[index++],
								result[index++],
								result[index++],
								result[index++],
								result[index++],
								result[index++],
								result[index++],
								result[index++],
								result[index++],
								result[index++]
						});

						// card Info protocol version
						cardInfo.setProtocolVersion(new byte[]{
								result[index++],
								result[index++],
						});

                        resultOptions.setCardInfo(cardInfo);

                        /////////////////////////////////////////////////////////////////////////
						// card Info encData
						ReaderResultOptions.CardInfo.ENCData encData = new ReaderResultOptions.CardInfo.ENCData();
						encData.setVanCount(result[index++]);
                        String datalen = new String(new byte[]{
                                result[index++],
                                result[index++],
                                result[index++],
                                result[index++],
                        });
                        encData.setVanEncDataLen(unicodeStringToInteger(datalen));

						// card Info encData van code
						encData.setVanCode(new byte[]{
								result[index++],
								result[index++],
								result[index++],
								result[index++],
						});

						// card Info encData key version
						encData.setVersion(new byte[]{
								result[index++],
								result[index++],
								result[index++],
								result[index++],
						});

                        // card Info encData enc type
						encData.setEncType(new byte[]{
								result[index++],
								result[index++],
						});

                        // card Info encData data len
						encData.setDataLen(Integer.valueOf(bytesToHex(new byte[]{
								result[index++],
								result[index++],
						}).replace(" ", "")));

                        // card Info encData ksn
						ByteArrayBuffer ksnBuffer = new ByteArrayBuffer(20);
						ksnBuffer.append(result, index, 20);
						encData.setKsn(new String(ksnBuffer.toByteArray()));
						index += 20;

                        // card Info encData ENC Data
						ByteArrayBuffer encDataBuffer = new ByteArrayBuffer(encData.getDataLen() - 20);
						encDataBuffer.append(result, index, encData.getDataLen() - 20);
						encData.setEncData(encDataBuffer.toByteArray());
						index += encData.getDataLen() - 20;

                        // card Info encData hash type
                        encData.setHashtype(result[index++]);

                        resultOptions.getCardInfo().setEncData(encData);

                        /////////////////////////////////////////////////////////////////////////
                        // chip data info(EMV)
                        int chipDataLen = Integer.valueOf(bytesToHex(new byte[]{result[index++], result[index++]}));
                        ByteArrayBuffer chipDataBuffer = new ByteArrayBuffer(chipDataLen);
                        chipDataBuffer.append(result, index - 2, chipDataLen + 2);
                        resultOptions.setChipData(chipDataBuffer.toByteArray());

					}

				}
			}
		}

		return resultOptions;
	}

    /** convert unicode string to integer
     * @param unicodeNumberString
     * @return
     */
    public static int unicodeStringToInteger(String unicodeNumberString) {
        String result = "0";
        for(int i = 0; i < unicodeNumberString.length(); i++){
            result += unicodeNumberString.charAt(i);
        }
        return Integer.valueOf(result);
    }


    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /** convert bytes to hex string
     * @param bytes
     * @return
     */
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];// new char[bytes.length * 2 + bytes.length];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            //hexChars[j * 3 + 2] = ' ';
        }
        return new String(hexChars);
    }

    /** 값을 환산해서 int로 처리
     * @param bytes
     * @return
     */
    public int bytesToInt(byte [] bytes){
		int mask = 0x000000ff;
		int result = 0x00000000;

		for(int i = 0; i < bytes.length; i++){
			int temp = bytes[i] & mask;
			for(int j = 0; j < bytes.length - i - 1; j++){
				temp = temp << 8;
			}
			result |= temp ;
		}
		return result;
	}

    private void DelayTime(int dwTimeMS) {
        //Thread.yield();
        long StartTime, CheckTime;

        if(0==dwTimeMS) {
            Thread.yield();
            return;
        }
        //Returns milliseconds running in the current thread
        StartTime = System.currentTimeMillis();
        do {
            CheckTime=System.currentTimeMillis();
            Thread.yield();
        } while( (CheckTime-StartTime)<=dwTimeMS);
    }

    /** String 숫자를 byte array형태로 변환
     * @param str
     * @return
     */
    public static byte[] strNumberToByteArray(String str){
        ByteArrayBuffer result = new ByteArrayBuffer(str.length());
        for(int i = 0; i < str.length(); i++){
            int num = Integer.valueOf(str.substring(i, i+1));
            result.append((byte)num);
        }
        return result.toByteArray();
    }

}
