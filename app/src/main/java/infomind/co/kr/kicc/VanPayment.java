package infomind.co.kr.kicc;

import java.io.UnsupportedEncodingException;

import infomind.co.kr.util.LogUtil;
import infomind.co.kr.util.StringTextUtils;
import kicc.module.KiccModule;

/**
 * Created by jwh0728 on 2017. 9. 19..
 */

public class VanPayment {

    public static final String HOST_IP = "203.233.72.21";
    public static final int HOST_PORT = 15700;
    public static final String COMM_CANCEL_ID = "KICC";
    public static final String TERMINAL_NO = "700081";
    public static String KICC_PATH = "/sdcard/kicc/";

    private int PAYMENT_INDEX = 1;

    /** 카드 결제르 서버에 요청한다
     * @param resultOptions
     * @param vanRequestOptions
     * @return
     */
    public VanResultOptions cardPayment(ReaderResultOptions resultOptions, VanRequestOptions vanRequestOptions){

        // 서버 전문 전송
        String tmpMsg = "";
        String emvdata = "19454D";

        String cardInfo = "SECU" + new String(resultOptions.getCardInfo().getModelCode()) +
                new String(resultOptions.getCardInfo().getVersion()) +
                new String(resultOptions.getCardInfo().getEncData().getEncType()) +
                resultOptions.getCardInfo().getEncData().getKsn() +
                new String(resultOptions.getCardInfo().getEncData().getEncData());

        vanRequestOptions.setS01MakerCode("EX");
        vanRequestOptions.setS02TextType("I1");
        vanRequestOptions.setS03Index(" ");
        vanRequestOptions.setS04TerminalDistCode("40");
        vanRequestOptions.setS05TerminalNo(StringTextUtils.paddingLeft(TERMINAL_NO, 7, ' '));
        vanRequestOptions.setS08Wcc("A");
        vanRequestOptions.setS09CardNo(cardInfo);

        tmpMsg = "S01=" + vanRequestOptions.getS01MakerCode() + ";" +
                "S02=" + vanRequestOptions.getS02TextType() + ";" +
                "S03=" + vanRequestOptions.getS03Index() + ";" +
                "S04=" + vanRequestOptions.getS04TerminalDistCode() + ";" +
                "S05=" + vanRequestOptions.getS05TerminalNo() + ";" +
                "S08=" + vanRequestOptions.getS08Wcc() + ";" +
                "S09=" + vanRequestOptions.getS09CardNo() + ";" +
                "S11=" + vanRequestOptions.getS11InstallmentMonths() + ";" +
                "S12=" + vanRequestOptions.getS12Amount() + ";" +
                "S17=" + vanRequestOptions.getS17ServiceCharge() + ";" +
                "S18=" + vanRequestOptions.getS18SurTax() + ";" +
                "S19=" + vanRequestOptions.getS19DscUseYN() + ";";

        LogUtil.d("tmpMsg", tmpMsg);
        if(resultOptions.getChipData() != null) {
            emvdata += CardReaderCommon.bytesToHex(resultOptions.getChipData());
        }
        LogUtil.d("emvData", emvdata);

        // POS 거래번호 증가
        PAYMENT_INDEX = (PAYMENT_INDEX > 9999 ? 1 : PAYMENT_INDEX++);

        VanResultOptions vanResultOptions = new VanResultOptions();
        try{
            String resultData = new String(
                    KiccModule.KiccApproval(3,tmpMsg,tmpMsg.length(),
                            3,"",emvdata, HOST_IP , HOST_PORT,0,
                            COMM_CANCEL_ID, TERMINAL_NO,StringTextUtils.paddingLeft(String.valueOf(PAYMENT_INDEX), 4, '0'),
                            KICC_PATH),
                    "EUC-KR");
            LogUtil.e("KICC RECV",resultData);

            vanResultOptions = parseVanResult(resultData);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        return vanResultOptions;
    }

    /** 카드 결제 취소를 요청한다
     * @param resultOptions
     * @param vanRequestOptions
     * @return
     */
    public VanResultOptions cardPaymentCancel(ReaderResultOptions resultOptions, VanRequestOptions vanRequestOptions){
        // 서버 전문 전송
        String tmpMsg = "";
        String emvdata = "19454D";

        String cardInfo = "SECU" + new String(resultOptions.getCardInfo().getModelCode()) +
                new String(resultOptions.getCardInfo().getVersion()) +
                new String(resultOptions.getCardInfo().getEncData().getEncType()) +
                resultOptions.getCardInfo().getEncData().getKsn() +
                new String(resultOptions.getCardInfo().getEncData().getEncData());

        vanRequestOptions.setS01MakerCode("EX");
        vanRequestOptions.setS02TextType("I2");
        vanRequestOptions.setS03Index(" ");
        vanRequestOptions.setS04TerminalDistCode("40");
        vanRequestOptions.setS05TerminalNo(StringTextUtils.paddingLeft(TERMINAL_NO, 7, ' '));
        vanRequestOptions.setS08Wcc("Q");
        vanRequestOptions.setS09CardNo(cardInfo);

        tmpMsg = "S01=" + vanRequestOptions.getS01MakerCode() + ";" +
                "S02=" + vanRequestOptions.getS02TextType() + ";" +
                "S03=" + vanRequestOptions.getS03Index() + ";" +
                "S04=" + vanRequestOptions.getS04TerminalDistCode() + ";" +
                "S05=" + vanRequestOptions.getS05TerminalNo() + ";" +
                "S08=" + vanRequestOptions.getS08Wcc() + ";" +
                "S09=" + vanRequestOptions.getS09CardNo() + ";" +
                "S11=" + vanRequestOptions.getS11InstallmentMonths() + ";" +
                "S12=" + vanRequestOptions.getS12Amount() + ";" +
                "S14=" + vanRequestOptions.getS14ApprovalNo() + ";" +
                "S17=" + vanRequestOptions.getS17ServiceCharge() + ";" +
                "S18=" + vanRequestOptions.getS18SurTax() + ";" +
                "S19=" + vanRequestOptions.getS19DscUseYN() + ";";

        LogUtil.d("tmpMsg", tmpMsg);
        emvdata += CardReaderCommon.bytesToHex(resultOptions.getChipData());
        LogUtil.d("emvData", emvdata);

        VanResultOptions vanResultOptions;
        try{
            String resultData = new String(
                    KiccModule.KiccApproval(3,tmpMsg,tmpMsg.length(),
                            3,"",emvdata, HOST_IP , HOST_PORT,0,
                            COMM_CANCEL_ID, TERMINAL_NO,StringTextUtils.paddingLeft(String.valueOf(PAYMENT_INDEX), 4, '0'),
                            KICC_PATH),
                    "EUC-KR");
            LogUtil.e("KICC RECV",resultData);

            vanResultOptions = parseVanResult(resultData);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        return vanResultOptions;
    }

    /** 현금 영수증 처리를 요청 한다
     * @param vanRequestOptions
     * @return
     */
    public VanResultOptions cashPayment(VanRequestOptions vanRequestOptions){
        // 서버 전문 전송
        String tmpMsg = "";

        vanRequestOptions.setS01MakerCode("EX");
        vanRequestOptions.setS02TextType("B1");
        vanRequestOptions.setS03Index(" ");
        vanRequestOptions.setS04TerminalDistCode("40");
        vanRequestOptions.setS05TerminalNo(StringTextUtils.paddingLeft(VanPayment.TERMINAL_NO, 7, '0'));
        vanRequestOptions.setS08Wcc("@");
        vanRequestOptions.setS19DscUseYN("N");

        tmpMsg = "S01=" + vanRequestOptions.getS01MakerCode() + ";" +
                "S02=" + vanRequestOptions.getS02TextType() + ";" +
                "S03=" + vanRequestOptions.getS03Index() + ";" +
                "S04=" + vanRequestOptions.getS04TerminalDistCode() + ";" +
                "S05=" + vanRequestOptions.getS05TerminalNo() + ";" +
                "S08=" + vanRequestOptions.getS08Wcc() + ";" +
                "S09=" + vanRequestOptions.getS09CardNo() + ";" +
                "S12=" + vanRequestOptions.getS12Amount() + ";" +
                "S13=" + vanRequestOptions.getS13CashAmount() + ";" +
                "S18=" + vanRequestOptions.getS18SurTax() + ";" +
                "S19=" + vanRequestOptions.getS19DscUseYN() + ";";

        LogUtil.d("tmpMsg", tmpMsg);

        VanResultOptions vanResultOptions;
        try{
            String resultData = new String(
                    KiccModule.KiccApproval(3,tmpMsg,tmpMsg.length(),
                            3,"","", HOST_IP , HOST_PORT,0,
                            COMM_CANCEL_ID, TERMINAL_NO,StringTextUtils.paddingLeft(String.valueOf(PAYMENT_INDEX), 4, '0'),
                            KICC_PATH),
                    "EUC-KR");
            LogUtil.e("KICC RECV",resultData);

            vanResultOptions = parseVanResult(resultData);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        return vanResultOptions;
    }

    /** 현금 영수증 취소 처리를 요청 한다
     * @param vanRequestOptions
     * @return
     */
    public VanResultOptions cashPaymentCancel(VanRequestOptions vanRequestOptions){
        // 서버 전문 전송
        String tmpMsg = "";

        vanRequestOptions.setS01MakerCode("EX");
        vanRequestOptions.setS02TextType("B2");
        vanRequestOptions.setS03Index(" ");
        vanRequestOptions.setS04TerminalDistCode("40");
        vanRequestOptions.setS05TerminalNo(StringTextUtils.paddingLeft(VanPayment.TERMINAL_NO, 7, '0'));
        vanRequestOptions.setS08Wcc("@");
        vanRequestOptions.setS11InstallmentMonths("1");
        vanRequestOptions.setS19DscUseYN("N");

        tmpMsg = "S01=" + vanRequestOptions.getS01MakerCode() + ";" +
                "S02=" + vanRequestOptions.getS02TextType() + ";" +
                "S03=" + vanRequestOptions.getS03Index() + ";" +
                "S04=" + vanRequestOptions.getS04TerminalDistCode() + ";" +
                "S05=" + vanRequestOptions.getS05TerminalNo() + ";" +
                "S08=" + vanRequestOptions.getS08Wcc() + ";" +
                "S09=" + vanRequestOptions.getS09CardNo() + ";" +
                "S12=" + vanRequestOptions.getS12Amount() + ";" +
                "S13=" + vanRequestOptions.getS13CashAmount() + ";" +
                "S14=" + vanRequestOptions.getS14ApprovalNo() + ";" +
                "S15=" + vanRequestOptions.getS15ApprovalDate() + ";" +
                "S18=" + vanRequestOptions.getS18SurTax() + ";" +
                "S19=" + vanRequestOptions.getS19DscUseYN() + ";";

        LogUtil.d("tmpMsg", tmpMsg);

        VanResultOptions vanResultOptions = new VanResultOptions();
        try{
            String resultData = new String(
                    KiccModule.KiccApproval(3,tmpMsg,tmpMsg.length(),
                            3,"","", HOST_IP , HOST_PORT,0,
                            COMM_CANCEL_ID, TERMINAL_NO,StringTextUtils.paddingLeft(String.valueOf(PAYMENT_INDEX), 4, '0'),
                            KICC_PATH),
                    "EUC-KR");
            LogUtil.e("KICC RECV",resultData);

            vanResultOptions = parseVanResult(resultData);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        return vanResultOptions;
    }

    /** 결제 요청 후 결과를 해석한다
     * @param resultData
     * @return
     */
    private VanResultOptions parseVanResult(String resultData){

        VanResultOptions vanResultOptions = new VanResultOptions();

        String [] resultArray = resultData.split(";");
        for(String result : resultArray){
            String key = result.split("=")[0];
            String value = "";
            if(result.split("=").length > 1){
                value =result.split("=")[1];
            }

            if(key.equals("R01")){
                vanResultOptions.setR01MakerCode(value);
            }else if(key.equals("R02")){
                vanResultOptions.setR02ControlCode(value);
            }else if(key.equals("R03")){
                vanResultOptions.setR03Index(value);
            }else if(key.equals("R04")){
                vanResultOptions.setR04PaymentNumber(value);
            }else if(key.equals("R05")){
                vanResultOptions.setR05TerminalType(value);
            }else if(key.equals("R06")){
                vanResultOptions.setR06TerminalNo(value);
            }else if(key.equals("R07")){
                vanResultOptions.setR07ResponseCode(value);
            }else if(key.equals("R08")){
                vanResultOptions.setR08Acquirer(value);
            }else if(key.equals("R09")){
                vanResultOptions.setR09AcquireIndex(value);
            }else if(key.equals("R10")){
                vanResultOptions.setR10ApprovalDate(value);
            }else if(key.equals("R12")){
                vanResultOptions.setR12ApprovalNo(value);
            }else if(key.equals("R15")){
                vanResultOptions.setR15IssuerCode(value);
            }else if(key.equals("R16")){
                vanResultOptions.setR16IssuerName(value);
            }else if(key.equals("R17")){
                vanResultOptions.setR17MerchantNo(value);
            }else if(key.equals("R18")){
                vanResultOptions.setR18AcquirerName(value);
            }else if(key.equals("R19")){
                vanResultOptions.setR19ScreenControlCode(value);
            }else if(key.equals("R20")){
                vanResultOptions.setR20ScreenContents(value);
            }else if(key.equals("R22")){
                vanResultOptions.setR22Notice(value);
            }else if(key.equals("R26")){
                vanResultOptions.setR26CardNo(value);
            }

        }

        return vanResultOptions;
    }
}
