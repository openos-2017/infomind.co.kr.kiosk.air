package infomind.co.kr.kicc;

/**
 * Created by jwh0728 on 2017. 9. 19..
 */

public class VanResultOptions {
    private String r01MakerCode = "";

    private String r02ControlCode = "";

    private String r03Index = "";

    private String r04PaymentNumber = "";

    private String r05TerminalType = "";

    private String r06TerminalNo = "";

    private String r07ResponseCode = "";

    private String r08Acquirer = "";

    private String r09AcquireIndex = "";

    private String r10ApprovalDate = "";

    private String r12ApprovalNo = "";

    private String r15IssuerCode = "";

    private String r16IssuerName = "";

    private String r17MerchantNo = "";

    private String r18AcquirerName = "";

    private String r19ScreenControlCode = "";

    private String r20ScreenContents = "";

    private String r22Notice= "";

    private String r26CardNo = "";

    public String getR01MakerCode() {
        return r01MakerCode;
    }

    public void setR01MakerCode(String r01MakerCode) {
        this.r01MakerCode = r01MakerCode;
    }

    public String getR02ControlCode() {
        return r02ControlCode;
    }

    public void setR02ControlCode(String r02ControlCode) {
        this.r02ControlCode = r02ControlCode;
    }

    public String getR03Index() {
        return r03Index;
    }

    public void setR03Index(String r03Index) {
        this.r03Index = r03Index;
    }

    public String getR04PaymentNumber() {
        return r04PaymentNumber;
    }

    public void setR04PaymentNumber(String r04PaymentNumber) {
        this.r04PaymentNumber = r04PaymentNumber;
    }

    public String getR05TerminalType() {
        return r05TerminalType;
    }

    public void setR05TerminalType(String r05TerminalType) {
        this.r05TerminalType = r05TerminalType;
    }

    public String getR06TerminalNo() {
        return r06TerminalNo;
    }

    public void setR06TerminalNo(String r06TerminalNo) {
        this.r06TerminalNo = r06TerminalNo;
    }

    public String getR07ResponseCode() {
        return r07ResponseCode;
    }

    public void setR07ResponseCode(String r07ResponseCode) {
        this.r07ResponseCode = r07ResponseCode;
    }

    public String getR08Acquirer() {
        return r08Acquirer;
    }

    public void setR08Acquirer(String r08Acquirer) {
        this.r08Acquirer = r08Acquirer;
    }

    public String getR09AcquireIndex() {
        return r09AcquireIndex;
    }

    public void setR09AcquireIndex(String r09AcquireIndex) {
        this.r09AcquireIndex = r09AcquireIndex;
    }

    public String getR10ApprovalDate() {
        return r10ApprovalDate;
    }

    public void setR10ApprovalDate(String r10ApprovalDate) {
        this.r10ApprovalDate = r10ApprovalDate;
    }

    public String getR12ApprovalNo() {
        return r12ApprovalNo;
    }

    public void setR12ApprovalNo(String r12ApprovalNo) {
        this.r12ApprovalNo = r12ApprovalNo;
    }

    public String getR15IssuerCode() {
        return r15IssuerCode;
    }

    public void setR15IssuerCode(String r15IssuerCode) {
        this.r15IssuerCode = r15IssuerCode;
    }

    public String getR16IssuerName() {
        return r16IssuerName;
    }

    public void setR16IssuerName(String r16IssuerName) {
        this.r16IssuerName = r16IssuerName;
    }

    public String getR17MerchantNo() {
        return r17MerchantNo;
    }

    public void setR17MerchantNo(String r17MerchantNo) {
        this.r17MerchantNo = r17MerchantNo;
    }

    public String getR18AcquirerName() {
        return r18AcquirerName;
    }

    public void setR18AcquirerName(String r18AcquirerName) {
        this.r18AcquirerName = r18AcquirerName;
    }

    public String getR19ScreenControlCode() {
        return r19ScreenControlCode;
    }

    public void setR19ScreenControlCode(String r19ScreenControlCode) {
        this.r19ScreenControlCode = r19ScreenControlCode;
    }

    public String getR20ScreenContents() {
        return r20ScreenContents;
    }

    public void setR20ScreenContents(String r20ScreenContents) {
        this.r20ScreenContents = r20ScreenContents;
    }

    public String getR22Notice() {
        return r22Notice;
    }

    public void setR22Notice(String r22Notice) {
        this.r22Notice = r22Notice;
    }

    public String getR26CardNo() {
        return r26CardNo;
    }

    public void setR26CardNo(String r26CardNo) {
        this.r26CardNo = r26CardNo;
    }
}
