package infomind.co.kr.kicc;

import infomind.co.kr.kicc.ReaderResultOptions;

import org.json.JSONException;

public interface OnReaderResultReceived {
	void returnReceivedData(ReaderResultOptions resultOptions, String parameter) throws JSONException;
}
