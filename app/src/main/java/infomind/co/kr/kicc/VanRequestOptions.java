package infomind.co.kr.kicc;

/**
 * Created by jwh0728 on 2017. 9. 19..
 */

public class VanRequestOptions {

    private String s01MakerCode = "";

    private String s02TextType = "";

    private String s03Index = "";

    private String s04TerminalDistCode = "";

    private String s05TerminalNo = "";

    private String s08Wcc = "";

    private String s09CardNo = "";

    private String s11InstallmentMonths = "";

    private String s12Amount = "";

    private String s13CashAmount = "";

    public String getS14ApprovalNo() {
        return s14ApprovalNo;
    }

    public void setS14ApprovalNo(String s14ApprovalNo) {
        this.s14ApprovalNo = s14ApprovalNo;
    }

    private String s14ApprovalNo = "";

    private String s15ApprovalDate = "";

    public String getS15ApprovalDate() {
        return s15ApprovalDate;
    }

    public void setS15ApprovalDate(String s15ApprovalDate) {
        this.s15ApprovalDate = s15ApprovalDate;
    }

    private String s17ServiceCharge = "";

    private String s18SurTax = "";

    private String s19DscUseYN = "N";

    public String getS01MakerCode() {
        return s01MakerCode;
    }

    public void setS01MakerCode(String s01MakerCode) {
        this.s01MakerCode = s01MakerCode;
    }

    public String getS02TextType() {
        return s02TextType;
    }

    public void setS02TextType(String s02TextType) {
        this.s02TextType = s02TextType;
    }

    public String getS03Index() {
        return s03Index;
    }

    public void setS03Index(String s03Index) {
        this.s03Index = s03Index;
    }

    public String getS04TerminalDistCode() {
        return s04TerminalDistCode;
    }

    public void setS04TerminalDistCode(String s04TerminalDistCode) {
        this.s04TerminalDistCode = s04TerminalDistCode;
    }

    public String getS05TerminalNo() {
        return s05TerminalNo;
    }

    public void setS05TerminalNo(String s05TerminalNo) {
        this.s05TerminalNo = s05TerminalNo;
    }

    public String getS08Wcc() {
        return s08Wcc;
    }

    public void setS08Wcc(String s08Wcc) {
        this.s08Wcc = s08Wcc;
    }

    public String getS11InstallmentMonths() {
        return s11InstallmentMonths;
    }

    public void setS11InstallmentMonths(String s11InstallmentMonths) {
        this.s11InstallmentMonths = s11InstallmentMonths;
    }

    public String getS12Amount() {
        return s12Amount;
    }

    public void setS12Amount(String s12Amount) {
        this.s12Amount = s12Amount;
    }

    public String getS13CashAmount() {
        return s13CashAmount;
    }

    public void setS13CashAmount(String s13CashAmount) {
        this.s13CashAmount = s13CashAmount;
    }

    public String getS17ServiceCharge() {
        return s17ServiceCharge;
    }

    public void setS17ServiceCharge(String s17ServiceCharge) {
        this.s17ServiceCharge = s17ServiceCharge;
    }

    public String getS18SurTax() {
        return s18SurTax;
    }

    public void setS18SurTax(String s18SurTax) {
        this.s18SurTax = s18SurTax;
    }

    public String getS19DscUseYN() {
        return s19DscUseYN;
    }

    public void setS19DscUseYN(String s19DscUseYN) {
        this.s19DscUseYN = s19DscUseYN;
    }

    public String getS09CardNo() {
        return s09CardNo;
    }

    public void setS09CardNo(String s09CardNo) {
        this.s09CardNo = s09CardNo;
    }
}
