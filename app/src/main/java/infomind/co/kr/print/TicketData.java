package infomind.co.kr.print;

/**
 * @author jwh0728
 * 티켓 정보 
 */
public class TicketData {
	/**
	 * 상품 상세명 
	 */
	private String productDetailNm = "";
	/**
	 * 상품 갯수 
	 */
	private int orderCnt = 0;
	/**
	 * 상품 최종 금액 
	 */
	private int orderPaymentPrice = 0;
	/**
	 * 상품 단가 
	 */
	private int orderDangaPrice = 0;
	
	public String getProductDetailNm() {
		return productDetailNm;
	}
	public void setProductDetailNm(String productDetailNm) {
		this.productDetailNm = productDetailNm;
	}
	public int getOrderCnt() {
		return orderCnt;
	}
	public void setOrderCnt(int orderCnt) {
		this.orderCnt = orderCnt;
	}
	public int getOrderPaymentPrice() {
		return orderPaymentPrice;
	}
	public void setOrderPaymentPrice(int orderPaymentPrice) {
		this.orderPaymentPrice = orderPaymentPrice;
	}
	public int getOrderDangaPrice() {
		return orderDangaPrice;
	}
	public void setOrderDangaPrice(int orderDangaPrice) {
		this.orderDangaPrice = orderDangaPrice;
	}
	
}