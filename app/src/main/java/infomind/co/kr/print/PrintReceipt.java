package infomind.co.kr.print;

import android.os.Handler;

import org.apache.http.util.ByteArrayBuffer;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;

import infomind.co.kr.data.ReturnResult;
import infomind.co.kr.net.MultiSerial;
import infomind.co.kr.util.LogUtil;

public class PrintReceipt {

    /**
     * 메시지 핸들러
     */
    private Handler handler = new Handler();

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

	/**
	 * 프린터 USB_SERIAL 번호
	 */
	private int deviceIndex;

    public int getDeviceIndex() {
        return deviceIndex;
    }

    public void setDeviceIndex(int deviceIndex) {
        this.deviceIndex = deviceIndex;
    }

    /** Constructor
	 * @param multiSerial
	 */
	public PrintReceipt(MultiSerial multiSerial, int deviceIndex){
		setMultiSerial(multiSerial);
		this.deviceIndex = deviceIndex;
	}

	/**
	 * Serial data
	 */
	private MultiSerial multiSerial;

	public MultiSerial getMultiSerial() {
		return multiSerial;
	}

	public void setMultiSerial(MultiSerial multiSerial) {
		this.multiSerial = multiSerial;
	}
	
	private OnPrinterResultReceived onResultReceived;

    public OnPrinterResultReceived getOnResultReceived() {
        return onResultReceived;
    }

    public void setOnResultReceived(OnPrinterResultReceived onResultReceived) {
        this.onResultReceived = onResultReceived;
    }


	/** 프린팅
	 * @param message
	 */
	public void printMessage(final String message, final String parameter){
		new Thread(new Runnable() {

			@Override
			public void run() {
				multiSerial.openUARTDevice(deviceIndex);

				ReturnResult result;
				byte[] command = {
						 	/* feed and paper cut */ 0x1d, 0x56, (byte)65,
							0x0a, 0x00
				};
				try {
					result = init(message, command);
					LogUtil.d("printMessage", result.toJSONString());

					if(result.isSuccess()){
						onResultReceived.returnReceivedData(result);
					}else{
						onResultReceived.returnReceivedData(null);
					}
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}).start();
	}

	/** printmessage등을 보낸다.
	 * @return
	 * @throws Exception
	 */
	private ReturnResult init(String message, byte[] command) throws Exception {
		try {
			ByteArrayBuffer buffer = new ByteArrayBuffer(message.getBytes("EUC-KR").length + command.length);
			buffer.append(message.getBytes("EUC-KR"), 0, message.getBytes("EUC-KR").length);
			buffer.append(command, 0, command.length);

			int res;
			if(message.length() > 0){
				res = getMultiSerial().getSerial().PL2303Write(deviceIndex, buffer.toByteArray());
				if( res<0 ) {
					LogUtil.d("init", "setup2: fail to controlTransfer: "+ res);
					return new ReturnResult(false, "error");
				}
			}
			Thread.sleep(300);

			return new ReturnResult(true, "ok");
		} catch (Exception e) {
			LogUtil.d("TCPSockets", "Exception");
			return new ReturnResult(false, "Exception");
		}
	}

	/**
	 * paddingRight
	 * @param src
	 * @param blank
	 * @return
	 */
	private String paddingRight(String src, String blank){
		String temp = src;
		try {
			for(int i = 0; i < blank.length() - temp.getBytes("EUC-KR").length; i++){
				src += " ";
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return src;
	}
	
	/**
	 * paddingLeft
	 * @param src
	 * @param blank
	 * @return
	 */
	private String paddingLeft(String src, String blank){
		String temp = src;
		for(int i = 0; i < blank.length() - temp.length(); i++){
			src = " " + src;
		}
		return src;
	}
	
	/**
	 * print 
	 * @param printData
	 * @return
	 */
	public String printTicket(PrintData printData){
		
		String message =
				commandSetAlign(1)
				+ commandDoubleStrike(true) + "입 장 권" + commandDoubleStrike(false) + "\r\n"
				+ commandSetAlign(1)
				+ commandDoubleStrike(true) + "Ticket" + commandDoubleStrike(false) + "\r\n"
				+ commandSetAlign(1)
				+ commandEmphasize(true) + "관람 완료시까지 보관하여 주시기 바랍니다." + commandEmphasize(false) + "\r\n"
				+ "\r\n"
				+ commandSetAlign(1)
				+ commandDoubleStrike(true) + printData.getProductCodeNm() + commandDoubleStrike(false) + "\r\n"
				+ commandSetAlign(1)
				+ "(영 수 증)\r\n"
				+ commandSetAlign(0)
				+ "-----------------------------------------------\r\n"
				+ commandEmphasize(true) 
				+ "상품유형         인원/수량      단가       금액" + "\r\n"
				+ commandEmphasize(false)
				+ "-----------------------------------------------\r\n"
				+ commandEmphasize(true);
		int sumCnt = 0;
		int totalSum = 0;
		for(TicketData ticketData : printData.getTicketDatas()){
			if(sumCnt != 0) message += "\r\n\r\n"; 
			sumCnt += ticketData.getOrderCnt() * 1;
			int sum = ticketData.getOrderPaymentPrice();
			totalSum += sum;
			message += paddingRight(ticketData.getProductDetailNm(), getBlank(14))
					+ paddingLeft(toNumberComma(ticketData.getOrderCnt()), getBlank(8))
					+ paddingLeft(toNumberComma(ticketData.getOrderDangaPrice()), getBlank(14)) 
					+ paddingLeft(toNumberComma(sum), getBlank(11));
		}
		
		message += "\r\n"
				+ "\r\n"
				+ commandEmphasize(false)
				+ "-----------------------------------------------\r\n"
				+ commandEmphasize(true)
				+ "소계" 
				+ paddingLeft(toNumberComma(sumCnt), getBlank(18)) 
				+ paddingLeft(toNumberComma(totalSum), getBlank(25)) + "\r\n"
				+ "\r\n"
				+ "-----------------------------------------------\r\n"
				+ commandEmphasize(true)
				+ "결제금액[카드]" 
				+ paddingLeft(toNumberComma(totalSum), getBlank(33)) + "\r\n"
				+ commandEmphasize(false)
				+ "-----------------------------------------------\r\n"
				+ "티켓번호 : " + printData.getTicketNo().substring(0,13) + "-" + printData.getTicketNo().substring(13) + "\r\n"
				+ "단말기번호 : " + printData.getDeviceNo() + "\r\n"
				+ "이용일자 : " + printData.getPaymentDate().substring(0,4) + "-" 
								+ printData.getPaymentDate().substring(4,6) + "-"
								+ printData.getPaymentDate().substring(6,8) + " "
								+ printData.getPaymentDate().substring(8,10) + ":"
								+ printData.getPaymentDate().substring(10,12) + ":"
								+ printData.getPaymentDate().substring(12,14) + "\r\n"
				+ "문의전화 : " + printData.getCompanyTel() + "\r\n" 
				+ "-----------------------------------------------\r\n"
				+ "관람시 유의사항" + "\r\n"
				+ "1. 본 입장권은 1회만 사용이 가능합니다." + "\r\n"
				+ "2. 어린이 동반 시 사고 및 시설물 파손 방지 등에 유의하시기 바랍니다." + "\r\n"
				+ "\r\n"
				+ "-----------------------------------------------\r\n"
				+ commandEmphasize(true) 
				+ "* 입장권 소지 시 혜택" + "\r\n"
				+ "영수증 갖고 중문면세점 오세요~" + "\r\n"
				+ "(10만원 이상 구매시 1만원 할인 / 일부품목 제외)" + "\r\n"
				+ "중문관광단지 컨벤션 센터 1층 064)780-7700" + "\r\n"
				+ "\r\n";
		
		return message;
	}
	
	private String getBlank(int i) {
		String temp = "";
		for(int k = 0; k < i; k++){
			temp += " ";
		}
		return temp;
	}

	public String printTicketToCheck(PrintData printData){
		
		String message = 
				"-----------------------------------------------\r\n"
				+ commandSetAlign(1) 
				+ commandEmphasize(true) 
				+ "[신용카드(승인)전표]"
				+ "\r\n"
				+ commandEmphasize(false) 
				+ commandSetAlign(0) 
				+ "-----------------------------------------------\r\n"
				+ "시  설  명 : " + printData.getCompanyName() + "\r\n"
				+ "대  표  자 : " + printData.getCompanyCEO() + "\r\n"
				+ "사업자번호 : " + printData.getCompanyID() + "\r\n"
				+ "주     소 : " + printData.getCompanyAddress() + "\r\n"
				+ "-----------------------------------------------\r\n"
				+ "카 드 사 : " + printData.getPayCardNm() + "\r\n"
				+ "카 드 명 : " + printData.getCardPurNm() + "\r\n"
				+ "승인일시 : " + printData.getPaymentDate().substring(0,4) + "-" 
								+ printData.getPaymentDate().substring(4,6) + "-"
								+ printData.getPaymentDate().substring(6,8) + " "
								+ printData.getPaymentDate().substring(8,10) + ":"
								+ printData.getPaymentDate().substring(10,12) + ":"
								+ printData.getPaymentDate().substring(12,14) + "\r\n"
				+ "카드번호 : " + printData.getCardNo() + "\r\n"
				+ "결제방법 : " + printData.getPayType() + "\r\n"
				+ "승인번호 : " + printData.getApprovalNo() + "\r\n"
				+ "-----------------------------------------------\r\n";
		int sumCnt = 0;
		int totalSum = 0;
		for(TicketData ticketData : printData.getTicketDatas()){
				sumCnt += ticketData.getOrderCnt() * 1;
				int sum = ticketData.getOrderPaymentPrice();
				totalSum += sum;
//				message += paddingRight(ticketData.getProductDetailNm(), getBlank(14))
//						+ paddingLeft(toNumberComma(ticketData.getOrderCnt()), getBlank(8))
//						+ paddingLeft(toNumberComma(ticketData.getOrderDangaPrice()), getBlank(12)) 
//						+ paddingLeft(toNumberComma(sum), getBlank(14)) + "\r\n";
		}
		
		message += commandEmphasize(true) 
				+ "승인금액 : "
				+ toNumberComma(totalSum)
				+ "\r\n"
				+ commandSetAlign(2)
				+ paddingLeft("[회원용]", getBlank(23))
				+ "\r\n"
				+ commandEmphasize(false)
				+ "-----------------------------------------------\r\n"
				+ "\r\n";
		
		return message;
	}

	/**
	 * comma 찍기 
	 * @param orderCnt
	 * @return
	 */
	private String toNumberComma(int orderCnt) {
		DecimalFormat df = new DecimalFormat("#,##0");
		return df.format(orderCnt);
	}

	/** 2배 글씨 쓰기 
	 * @param bSet
	 * @return
	 */
	private String commandDoubleStrike(boolean bSet){
		if(bSet){
			byte [] command = {0x1b, 0x47, 0x01};
			return new String(command);
		}else{
			byte [] command = {0x1b, 0x47, 0x00};
			return new String(command);
		}
	}
	
	/** 강조모드 
	 * @param bSet
	 * @return
	 */
	private String commandEmphasize(boolean bSet){
		if(bSet){
			byte [] command = {0x1b, 0x45, 0x01};
			return new String(command);
		}else{
			byte [] command = {0x1b, 0x45, 0x00};
			return new String(command);
		}
	}
	
	/** 정렬 방식 
	 * @param align : 0 = left, 1 = center, 2 = right
	 * @return
	 */
	private String commandSetAlign(int align){
		byte [] command = {0x1b, 0x61, (byte)align};
		return new String(command);
	}

	private String commandRotateClockwise(boolean bSet){
		if(bSet){
			byte [] command = {0x1b, 0x56, 0x01};
			return new String(command);
		}else{
			byte [] command = {0x1b, 0x56, 0x00};
			return new String(command);
		}
	}
}
