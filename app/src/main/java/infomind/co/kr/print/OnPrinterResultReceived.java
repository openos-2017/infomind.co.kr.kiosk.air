package infomind.co.kr.print;

import org.json.JSONException;

import infomind.co.kr.data.ReturnResult;
import infomind.co.kr.kicc.ReaderResultOptions;

public interface OnPrinterResultReceived {
	void returnReceivedData(ReturnResult returnResult) throws JSONException;
}
