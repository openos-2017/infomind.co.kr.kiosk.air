package infomind.co.kr.print;

import java.util.ArrayList;

/**
 * @author jwh0728
 * 인쇄 기본 내용 
 */
public class PrintData{
	/**
	 * 상품명
	 */
	private String productCodeNm = "";
	/**
	 * 이용일자 
	 */
	private String startDd = "";
	/**
	 * 예매번호 
	 */
	private String orderSno = "";
	/**
	 * 승인번호 
	 */
	private String approvalNo = "";
	/**
	 * 승인일시 
	 */
	private String paymentDate = "";
	/**
	 * 카드사명 
	 */
	private String payCardNm = "";
	/**
	 * 카드명
	 */
	private String cardPurNm = "";
	/**
	 * 장비번호(단말기번호)
	 */
	private String deviceNo = "";
	/**
	 * 티켓번호 
	 */
	private String ticketNo = "";
	/**
	 * 카드번호 
	 */
	private String cardNo = "";
	
	private String companyName = "천지연 폭포";
	private String companyCEO = "제주특별자치도지사";
	private String companyID = "616-83-00017";
	private String companyAddress = "제주 서귀포시 서홍동 666-1";
	private String companyTel = "064-733-1528";
	private String payType = "일시불";
	
	
	/**
	 * 티켓 리스트 
	 */
	private ArrayList<TicketData> ticketDatas = new ArrayList<TicketData>();

	public String getProductCodeNm() {
		return productCodeNm;
	}

	public void setProductCodeNm(String productCodeNm) {
		this.productCodeNm = productCodeNm;
	}

	public String getStartDd() {
		return startDd;
	}

	public void setStartDd(String startDd) {
		this.startDd = startDd;
	}

	public String getOrderSno() {
		return orderSno;
	}

	public void setOrderSno(String orderSno) {
		this.orderSno = orderSno;
	}

	public String getApprovalNo() {
		return approvalNo;
	}

	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPayCardNm() {
		return payCardNm;
	}

	public void setPayCardNm(String payCardNm) {
		this.payCardNm = payCardNm;
	}

	public ArrayList<TicketData> getTicketDatas() {
		return ticketDatas;
	}

	public void setTicketDatas(ArrayList<TicketData> ticketDatas) {
		this.ticketDatas = ticketDatas;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCEO() {
		return companyCEO;
	}

	public void setCompanyCEO(String companyCEO) {
		this.companyCEO = companyCEO;
	}

	public String getCompanyID() {
		return companyID;
	}

	public void setCompanyID(String companyID) {
		this.companyID = companyID;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyTel() {
		return companyTel;
	}

	public void setCompanyTel(String companyTel) {
		this.companyTel = companyTel;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getDeviceNo() {
		return deviceNo;
	}

	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getCardPurNm() {
		return cardPurNm;
	}

	public void setCardPurNm(String cardPurNm) {
		this.cardPurNm = cardPurNm;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
}