package infomind.co.kr.kiosk.air;

import infomind.co.kr.data.DBCommonAndroid;
import infomind.co.kr.data.ReturnResult;
import infomind.co.kr.dialog.DialogProgressBar;
import infomind.co.kr.dialog.DialogSignView;
import infomind.co.kr.kicc.CardReader;
import infomind.co.kr.kicc.ReaderResultOptions;
import infomind.co.kr.kicc.ReaderRequestOptions;
import infomind.co.kr.print.OnPrinterResultReceived;
import infomind.co.kr.util.LogUtil;
import infomind.co.kr.util.StringTextUtils;
import infomind.co.kr.kicc.VanPayment;
import infomind.co.kr.kicc.VanRequestOptions;
import infomind.co.kr.kicc.VanResultOptions;
import infomind.co.kr.kis.NetworkChangeReceiver;
import infomind.co.kr.kicc.OnReaderResultReceived;
import infomind.co.kr.net.MultiSerial;
import infomind.co.kr.net.NetworkUtil;
import infomind.co.kr.print.PrintReceipt;
import infomind.co.kr.wechat.WeChatPayment;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TimerTask;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

@SuppressLint("SetJavaScriptEnabled")
public class MainActivity extends Activity {

    private String PREFERENCE = "infomind.co.kr.kiosk";
    private String LOG_TAG = MainActivity.class.getName();

    private Button cardPayment;
    private Button cardPaymentCancel;
    private Button cashPayment;
    private Button cashPaymentCancel;
    private Button print;
    private Button findCardDevice;

    private DBCommonAndroid db;

    /**
     * 결제 모듈
     */
    private CardReader cardReader = null;
    private WeChatPayment weChatPayment = null;

    /**
     * Multi Serial Communication
     */
    private MultiSerial multiSerial;
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case CardReader.DEVICE_CONNECTED:
                    LogUtil.d("handleMessage", "장비 접속");
                    break;
                case CardReader.NO_DEVICES:
                    LogUtil.d("handleMessage", "장비를 연결해 주세요.");
                    Toast.makeText(getApplicationContext(), "장비를 연결해 주세요.", Toast.LENGTH_LONG).show();
                    break;
                case CardReader.FIND_CARD_DEVICE_END:
                    LogUtil.d("handleMessage", "검색 완료");
                    Toast.makeText(getApplicationContext(), "검색 완료", Toast.LENGTH_LONG).show();

                    // set old value to preference
                    prefSet("CARD_READER_INDEX", String.valueOf(MultiSerial.CARD_READER_INDEX));
                    prefSet("TICKET_PRINTER_INDEX", String.valueOf(MultiSerial.TICKET_PRINTER_INDEX));
                    prefSet("RECEIPT_PRINTER_INDEX", String.valueOf(MultiSerial.RECEIPT_PRINTER_INDEX));

                    if (cardReader != null && multiSerial.getSerial() != null) {
                        for(int i = 0; i < multiSerial.getSerial().PL2303getProlificUSBDeviceCount(); i++){
                            multiSerial.getSerial().PL2303CloseDeviceByIndex(i);
                        }
                        multiSerial.getSerial().PL2303Release();
                        multiSerial.setSerial(null);
                        multiSerial = null;
                    }

//                    loadSerialDevices();
//
//                    printTest();
                    break;
                case CardReader.IC_FAIL_WAIT_MSR:
                    LogUtil.d("handleMessage", "IC 카드 읽기 실패 / MSR로 다시 읽어 주세요.");
                    Toast.makeText(getApplicationContext(), "IC 카드 읽기 실패 / MSR로 다시 읽어 주세요.", Toast.LENGTH_LONG).show();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /**
     * print module
     */
    private PrintReceipt printReceipt = null;
    private PrintReceipt printTicket = null;

    /**
     * timer Array
     */
    private ArrayList<TimerTask> timerTaskArray = new ArrayList<TimerTask>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardPayment = (Button) findViewById(R.id.cardPayment);
        cardPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCardData(1004, "00");
            }
        });

        cardPaymentCancel = (Button) findViewById(R.id.cardPaymentCancel);
        cardPaymentCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCardCancelData("24854659", 1004, "00");
            }
        });

        cashPayment = (Button) findViewById(R.id.cashPayment);
        cashPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCashData("01073020728", 1004, "00");
            }
        });

        cashPaymentCancel = (Button) findViewById(R.id.cashPaymentCancel);
        cashPaymentCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCashCancelData("01073020728", "148728118", 1004, "00", "170922");
            }
        });

        print = (Button) findViewById(R.id.print);
        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printTest();
            }
        });

        findCardDevice = (Button) findViewById(R.id.findCardDevice);
        findCardDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardReader.findCardDevices();
            }
        });

        try {
            db = new DBCommonAndroid(getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 결제용 로그 폴더 생성
        MakeFolder();

        // get old value from preference
        String CARD_READER_INDEX = prefGet("CARD_READER_INDEX");
        String TICKET_PRINTER_INDEX = prefGet("TICKET_PRINTER_INDEX");
        String RECEIPT_PRINTER_INDEX = prefGet("RECEIPT_PRINTER_INDEX");

        if(!CARD_READER_INDEX.equals("")) MultiSerial.CARD_READER_INDEX = Integer.valueOf(CARD_READER_INDEX);
        if(!TICKET_PRINTER_INDEX.equals("")) MultiSerial.TICKET_PRINTER_INDEX = Integer.valueOf(TICKET_PRINTER_INDEX);
        if(!RECEIPT_PRINTER_INDEX.equals("")) MultiSerial.RECEIPT_PRINTER_INDEX = Integer.valueOf(RECEIPT_PRINTER_INDEX);

        loadSerialDevices();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
//                getPosMSRInfo();
                //requestCardData();
                //multiSerial.findDevices();
                //cardReader.findCardDevices();

            }
        }, 3000);

    }

    private void loadSerialDevices(){
        multiSerial = new MultiSerial();
        multiSerial.initilalSerial(getApplicationContext());

        // 결제 모듈 로딩
        cardReader = new CardReader(getApplicationContext(), multiSerial);
        if (cardReader != null)
            cardReader.setHandler(handler);

        printTicket = new PrintReceipt(multiSerial, MultiSerial.TICKET_PRINTER_INDEX);
        if (printTicket != null)
            printTicket.setHandler(handler);

        printReceipt = new PrintReceipt(multiSerial, MultiSerial.RECEIPT_PRINTER_INDEX);
        if (printReceipt != null)
            printReceipt.setHandler(handler);
    }

    private void printTest() {
        printTicket.setOnResultReceived(new OnPrinterResultReceived() {
            @Override
            public void returnReceivedData(ReturnResult returnResult) throws JSONException {
                if(returnResult.isSuccess()) LogUtil.d("print", "ok");
            }
        });
        printTicket.printMessage("123456789012345678901234567890123456\n" +
                "동해물과 백두산이 마르고 닳도록\n" +
                "하느님이 보우하사 우리나라 만세\n" +
                "무궁화 삼천리 화려강산 \n" +
                "대한사람 대한으로 길이 보전하세.\n" +
                "Ticket Printer\n", "");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                printReceipt.setOnResultReceived(new OnPrinterResultReceived() {
                    @Override
                    public void returnReceivedData(ReturnResult returnResult) throws JSONException {
                        if(returnResult.isSuccess()) LogUtil.d("print", "ok");
                    }
                });
                printReceipt.printMessage("123456789012345678901234567890123456\n" +
                        "동해물과 백두산이 마르고 닳도록\n" +
                        "하느님이 보우하사 우리나라 만세\n" +
                        "무궁화 삼천리 화려강산 \n" +
                        "대한사람 대한으로 길이 보전하세.\n" +
                        "Receipt Printer\n", "");
            }
        }, 3000);
    }

    /**
     * 카드 결제 요청
     *
     * @param amount
     */
    private void requestCardData(final int amount, final String installmentMonths) {

        // IC 카드 데이터 요청 정보 작성
        ReaderRequestOptions.RequestCardData requestCardData = new ReaderRequestOptions.RequestCardData();
        // 거래 종류 (0: 승인, 1:취소)
        requestCardData.setTransactionType((byte) '0');
        // 거래금액
        requestCardData.setTransactionAmount(StringTextUtils.paddingLeft(String.valueOf(amount), 9, ' ').getBytes());

        cardReader.setOnReaderResultReceived(new OnReaderResultReceived() {

            @Override
            public void returnReceivedData(final ReaderResultOptions resultOptions, String parameter) throws JSONException {
                if (resultOptions != null) {
                    // 결과
                    LogUtil.d("returnReceivedData", "ok");

                    VanPayment vanPayment = new VanPayment();
                    VanRequestOptions vanRequestOptions = new VanRequestOptions();
                    vanRequestOptions.setS11InstallmentMonths(installmentMonths);                // 할부개월
                    vanRequestOptions.setS12Amount(String.valueOf(amount));         // 금액
                    vanRequestOptions.setS17ServiceCharge(String.valueOf(0));       // 봉사료
                    vanRequestOptions.setS18SurTax(String.valueOf(amount / 10));    // 부가세

                    // S19=Y 일 때 sign.bmp처럼 서명파일명 대입 파일명을 파라메터로 대입
                    // 서명파일의 포멧은 1bit 단색 bmp (128 * 64)

                    VanResultOptions vanResultOptions = vanPayment.cardPayment(resultOptions, vanRequestOptions);

                } else {

                }
            }
        });

        cardReader.getReaderCardData(requestCardData, "");

    }

    /**
     * 카드 결제 취소 처리 요청
     *
     * @param approvalNo
     * @param amount
     */
    private void requestCardCancelData(final String approvalNo, final int amount, final String installmentState) {

        // IC 카드 데이터 요청 정보 작성
        ReaderRequestOptions.RequestCardData requestCardData = new ReaderRequestOptions.RequestCardData();
        // 거래 종류 (0: 승인, 1:취소)
        requestCardData.setTransactionType("1".getBytes()[0]);
        // 거래금액
        requestCardData.setTransactionAmount(StringTextUtils.paddingLeft(String.valueOf(amount), 9, ' ').getBytes());

        cardReader.setOnReaderResultReceived(new OnReaderResultReceived() {

            @Override
            public void returnReceivedData(final ReaderResultOptions resultOptions, String parameter) throws JSONException {
                if (resultOptions != null) {
                    // 결과
                    LogUtil.d("returnReceivedData", "ok");

                    VanPayment vanPayment = new VanPayment();
                    VanRequestOptions vanRequestOptions = new VanRequestOptions();
                    vanRequestOptions.setS11InstallmentMonths(installmentState);            // 할부 개월수
                    vanRequestOptions.setS12Amount(String.valueOf(amount));                 // 금액
                    vanRequestOptions.setS14ApprovalNo(approvalNo);                         // 승인번호
                    vanRequestOptions.setS17ServiceCharge(String.valueOf(0));               // 봉사료
                    vanRequestOptions.setS18SurTax(String.valueOf(amount / 10));            // 부가세
                    VanResultOptions vanResultOptions = vanPayment.cardPaymentCancel(resultOptions, vanRequestOptions);

                } else {

                }
            }
        });

        cardReader.getReaderCardData(requestCardData, null);

    }

    /**
     * 현금 영수증 처리 요청
     *
     * @param amount
     */
    private void requestCashData(String cardNo, int amount, String type) {

        VanPayment vanPayment = new VanPayment();

        VanRequestOptions vanRequestOptions = new VanRequestOptions();
        vanRequestOptions.setS09CardNo(cardNo);                             // 현금영수증 번호
        vanRequestOptions.setS12Amount(String.valueOf(amount));             // 금액
        vanRequestOptions.setS13CashAmount(type);                           // 00 : 개인 , 01 : 사업자
        vanRequestOptions.setS18SurTax(String.valueOf(amount / 10));        // 부가세

        VanResultOptions vanResultOptions = vanPayment.cashPayment(vanRequestOptions);

        if (vanResultOptions.getR07ResponseCode().equals("0000")) {
            LogUtil.d(LOG_TAG, "success");
        } else {
            LogUtil.d(LOG_TAG, vanResultOptions.getR07ResponseCode() + " : " + vanResultOptions.getR20ScreenContents());
        }

    }

    /**
     * 현금 영수증 처리 요청
     *
     * @param amount
     */
    private void requestCashCancelData(String cardNo, String approvalNo, int amount, String type, String approvalDate) {

        VanPayment vanPayment = new VanPayment();

        VanRequestOptions vanRequestOptions = new VanRequestOptions();
        vanRequestOptions.setS09CardNo(cardNo);                             // 현금영수증 번호
        vanRequestOptions.setS12Amount(String.valueOf(amount));             // 금액
        vanRequestOptions.setS13CashAmount(type);                           // 00 : 개인 , 01 : 사업자
        vanRequestOptions.setS14ApprovalNo(approvalNo);                     // 승인번호
        vanRequestOptions.setS15ApprovalDate(approvalDate);                 // 승인일자
        vanRequestOptions.setS18SurTax(String.valueOf(amount / 10));        // 부가세

        VanResultOptions vanResultOptions = vanPayment.cashPaymentCancel(vanRequestOptions);

        if (vanResultOptions.getR07ResponseCode().equals("0000")) {
            LogUtil.d(LOG_TAG, "success");
        } else {
            LogUtil.d(LOG_TAG, vanResultOptions.getR07ResponseCode() + " : " + vanResultOptions.getR20ScreenContents());
        }

    }

    private void getPosMSRInfo() {
        // 송신 내용
        cardReader.setOnReaderResultReceived(new OnReaderResultReceived() {

            @Override
            public void returnReceivedData(final ReaderResultOptions resultOptions, String parameter) throws JSONException {
                if (resultOptions != null) {
                    // 결과
                    LogUtil.d("returnReceivedData", "ok");

                } else {

                }
            }
        });
        cardReader.getReaderPosMSRInfo("");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        LogUtil.d("onResume", "Enter onResume");
        super.onResume();
        String action = getIntent().getAction();
        LogUtil.d("onResume", "onResume:" + action);
        if (multiSerial != null) {
            if (multiSerial.getSerial().PL2303Enumerate() == 0) {
                Toast.makeText(this, "no more devices found", Toast.LENGTH_SHORT).show();
                return;
            } else {
                LogUtil.d("onResume", "onResume:enumerate succeeded!");
            }
            Toast.makeText(this, "attached", Toast.LENGTH_SHORT).show();
        }// if isConnected
        LogUtil.d("onResume", "Leave onResume");

        // network receiver
        IntentFilter intentFilter = new IntentFilter();
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        LogUtil.d("onDestroy", "Enter onDestroy");
        if (cardReader != null && multiSerial.getSerial() != null) {
            multiSerial.getSerial().PL2303Release();
            multiSerial.setSerial(null);
        }

        // timer task cancel
        for (TimerTask timerTask : timerTaskArray) {
            timerTask.cancel();
        }

        // unregister
        unregisterReceiver(networkChangeReceiver);

        super.onDestroy();
        LogUtil.d("onDestroy", "Leave onDestroy");
    }

    /**
     * 로딩바 표시
     */
    private DialogProgressBar dialogProgressBar = null;

    protected void showProgressBar(final Boolean bShow) {
        // progressbar
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (bShow) {
                    if (dialogProgressBar == null)
                        dialogProgressBar = new DialogProgressBar(MainActivity.this);
                    try {
                        dialogProgressBar.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        // TODO: handle exception
                    }
                } else {
                    try {
                        dialogProgressBar.dismiss();
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }
        });
    }

    /**
     * 5만원이상 구매를 위한 사인 처리 닫힐때 사인한 내용을 저장한다.
     */
    private DialogSignView dialogSignView = null;

    /**
     * @param bShow
     * @param approvalNo
     * @param requestMessage
     * @param retryMessage
     * @param dissmissListner
     */
    protected void showSignView(final Boolean bShow, final String approvalNo, final String requestMessage,
                                final String retryMessage, final OnDismissListener dissmissListner) {
        // progressbar
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (bShow) {
                    try {
                        if (dialogSignView == null)
                            dialogSignView = new DialogSignView(MainActivity.this,
                                    approvalNo,
                                    requestMessage,
                                    retryMessage);
                        dialogSignView.show();
                        dialogSignView.setOnDismissListener(dissmissListner);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        dialogSignView.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * hide Navigation & status bar
     */
    private void hideNavigationStatusBar() {
        try {
            Process su = Runtime.getRuntime().exec("su");
            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

            outputStream.writeBytes("service call activity 42 s16 com.android.systemui\n");
            outputStream.flush();

            outputStream.writeBytes("exit\n");
            outputStream.flush();
            su.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * show Navigation & status bar
     */
    private void showNavigationStatusBar() {
        try {
            Process su = Runtime.getRuntime().exec("su");
            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

            outputStream.writeBytes("am startservice -n com.android.systemui/.SystemUIService\n");
            outputStream.flush();

            outputStream.writeBytes("exit\n");
            outputStream.flush();
            su.waitFor();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /**
     * 네트워크 변경 리시버
     */
    private NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {

            String status = NetworkUtil.getConnectivityStatusString(context);

            Toast.makeText(context, status, Toast.LENGTH_LONG).show();
            LogUtil.d("NetworkChangeReceiver", status);

            if (NetworkUtil.getConnectivityStatus(context) == NetworkUtil.TYPE_NOT_CONNECTED) {
                // showNetworkErrorPage(true);
            } else {
                // showNetworkErrorPage(false);
            }
        }
    };


    /**
     * kicc 결제 통신 모듈 로그 폴더 생성
     */
    private void MakeFolder() {

        File path = new File("/sdcard/kicc/LOG");

        if (!path.isDirectory()) {

            path.mkdirs();

        }
        File path_trlog = new File("/sdcard/kicc/TRLOG");

        if (!path_trlog.isDirectory()) {

            path_trlog.mkdirs();

        }
    }

    /**
     * 값을 설정.
     *
     * @param keyString
     * @param valueString
     */
    public void prefSet(String keyString, String valueString) {
        SharedPreferences prefs = getSharedPreferences(PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(keyString, valueString);
        editor.apply();
    }

    /**
     * 값을 가져옴. 보통 키값을 저장하기 위함. 값이 없으면 ""를 리턴
     *
     * @param keyString
     * @return
     */
    public String prefGet(String keyString) {
        SharedPreferences prefs = getSharedPreferences(PREFERENCE, MODE_PRIVATE);
        return prefs.getString(keyString, "");
    }
}
