package infomind.co.kr.net;

import java.net.URI;
import java.net.URISyntaxException;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import android.os.Build;

import infomind.co.kr.util.LogUtil;

public class WebSocket {
	private WebSocketClient mWebSocketClient;
	private void connectWebSocket() {
		URI uri;
		try {
			uri = new URI("ws://192.168.0.2:8080/sgposm/socketServerEndpoint");
		} catch (URISyntaxException e) {
			e.printStackTrace();
			return;
		}

		mWebSocketClient = new WebSocketClient(uri, new Draft_17()) {
			@Override
			public void onOpen(ServerHandshake serverHandshake) {
				LogUtil.i("Websocket", "Opened");
				mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
			}

			@Override
			public void onMessage(String s) {
				final String message = s;
				LogUtil.i("onMessage", message);
//				runOnUiThread(new Runnable() {
//					
//					@Override
//					public void run() {
//						Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
//					}
//				});
			}

			@Override
			public void onClose(int i, String s, boolean b) {
				LogUtil.i("Websocket", "Closed " + s);
			}

			@Override
			public void onError(Exception e) {
				LogUtil.i("Websocket", "Error " + e.getMessage());
			}
		};
		mWebSocketClient.connect();
	}
}
