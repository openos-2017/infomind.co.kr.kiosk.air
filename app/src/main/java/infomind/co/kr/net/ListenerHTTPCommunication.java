package infomind.co.kr.net;

/**
 * HTTPCommunication listener
 * @author jwh0728
 */
public interface ListenerHTTPCommunication {
	/** Return result when received 
	 * @param result
	 */
	public void receiveResult(String result);
}
