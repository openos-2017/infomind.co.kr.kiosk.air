package infomind.co.kr.net;

import android.content.Context;
import android.hardware.usb.UsbManager;

import infomind.co.kr.util.LogUtil;
import tw.com.prolific.pl2303multilib.PL2303MultiLib;

/**
 * Created by jwh0728 on 2017. 8. 25..
 */

public class MultiSerial {

    class UARTSettingInfo {
        public int iPortIndex = 0;
        public PL2303MultiLib.BaudRate mBaudrate = PL2303MultiLib.BaudRate.B57600;
        public PL2303MultiLib.DataBits mDataBits = PL2303MultiLib.DataBits.D8;
        public PL2303MultiLib.Parity mParity = PL2303MultiLib.Parity.NONE;
        public PL2303MultiLib.StopBits mStopBits = PL2303MultiLib.StopBits.S1;
        public PL2303MultiLib.FlowControl mFlowControl = PL2303MultiLib.FlowControl.OFF;
    }// class UARTSettingInfo

    public static int CARD_READER_INDEX = 0;
    public static int TICKET_PRINTER_INDEX = 1;
    public static int RECEIPT_PRINTER_INDEX = 2;

    private Context context;
    private static final int MAX_DEVICE_COUNT = 4;
    private static final String ACTION_USB_PERMISSION = "infomind.co.kr.kiosk.air1.USB_PERMISSION";

    private UARTSettingInfo gUARTInfoList[];

    public UARTSettingInfo[] getgUARTInfoList() {
        return gUARTInfoList;
    }

    public void setgUARTInfoList(UARTSettingInfo[] gUARTInfoList) {
        this.gUARTInfoList = gUARTInfoList;
    }

    private boolean bDeviceOpened[] = new boolean[MAX_DEVICE_COUNT];

    // serial
    private PL2303MultiLib mSerialMulti;
    public PL2303MultiLib getSerial() {
        return mSerialMulti;
    }

    public void setSerial(PL2303MultiLib serial) {
        this.mSerialMulti = serial;
    }

    public void initilalSerial(Context context){
        // get service
        mSerialMulti = new PL2303MultiLib(	(UsbManager) context.getSystemService(Context.USB_SERVICE),
                context,
                ACTION_USB_PERMISSION);
        // if you don't want to use Software Queue, below constructor to be used
        // mSerialMulti = new PL2303MultiLib((UsbManager)
        // getSystemService(Context.USB_SERVICE),
        // this, ACTION_USB_PERMISSION,false);

        gUARTInfoList = new UARTSettingInfo[MAX_DEVICE_COUNT];

        for (int i = 0; i < MAX_DEVICE_COUNT; i++) {
            gUARTInfoList[i] = new UARTSettingInfo();
            gUARTInfoList[i].iPortIndex = i;
            bDeviceOpened[i] = false;
            if(i == CARD_READER_INDEX) {
                gUARTInfoList[i].mBaudrate = PL2303MultiLib.BaudRate.B57600;
                gUARTInfoList[i].mFlowControl = PL2303MultiLib.FlowControl.OFF;
            }else if(i == TICKET_PRINTER_INDEX){
                gUARTInfoList[i].mBaudrate = PL2303MultiLib.BaudRate.B9600;
                gUARTInfoList[i].mFlowControl = PL2303MultiLib.FlowControl.DTRDSR;
            }else if(i == RECEIPT_PRINTER_INDEX){
                gUARTInfoList[i].mBaudrate = PL2303MultiLib.BaudRate.B9600;
                gUARTInfoList[i].mFlowControl = PL2303MultiLib.FlowControl.DTRDSR;
            }
        }

    }

    /** open UART device
     * @param index
     */
    public void openUARTDevice(int index) {
        openUARTDevice(index, null);
    }

    public void openUARTDevice(int index, UARTSettingInfo info) {

        if(mSerialMulti==null)
            return;

        if(!mSerialMulti.PL2303IsDeviceConnectedByIndex(index))
            return;

        if(bDeviceOpened[index])
            return;

        if(info == null) info = gUARTInfoList[index];

        boolean res;
        res = mSerialMulti.PL2303OpenDevByUARTSetting(index, info.mBaudrate, info.mDataBits, info.mStopBits,
                info.mParity, info.mFlowControl);
        if( !res ) {
            LogUtil.e("MultiSerial", "Can't set UART correctly!");
            return;
        }

        bDeviceOpened[index] = true;
        LogUtil.d("MultiSerial", mSerialMulti.PL2303getDevicePathByIndex(index));

        return;
    }//private void OpenUARTDevice(int index

}
