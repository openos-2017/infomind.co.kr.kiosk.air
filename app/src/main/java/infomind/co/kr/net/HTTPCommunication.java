package infomind.co.kr.net;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.DateUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import infomind.co.kr.util.LogUtil;

/**
 * @author jwh0728
 */
public class HTTPCommunication {

	/**
	 * to use log
	 */
	protected static final String LOG_TAG = HTTPCommunication.class.getName();
	/**
	 * listener for result
	 */
	private ListenerHTTPCommunication listenerHTTPCommunication;
	/**
	 * timeout seconds
	 */
	protected static final int TIME_OUT = 60000;

	/**
	 * get listener
	 * 
	 * @return
	 */
	public ListenerHTTPCommunication getListenerHTTPCommunication() {
		return listenerHTTPCommunication;
	}

	/**
	 * set listener
	 * 
	 * @param listenerHTTPCommunication
	 */
	public void setListenerHTTPCommunication(ListenerHTTPCommunication listenerHTTPCommunication) {
		this.listenerHTTPCommunication = listenerHTTPCommunication;
	}

	/**
	 * Constructor method
	 */
	public HTTPCommunication() {
		super();
	}

	/**
	 * send and get result
	 * 
	 * @param uRLString
	 */
	public void sendHTTP(final String uRLString) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				HttpClient client = new DefaultHttpClient();
				client.getParams().setParameter("http.protocol.expect-continue", false);
				client.getParams().setParameter("http.connection.timeout", TIME_OUT);
				client.getParams().setParameter("http.socket.timeout", TIME_OUT);
				// String getURL = "http://localhost";

				HttpGet get = new HttpGet(uRLString);
				HttpResponse responseGet;
				try {
					responseGet = client.execute(get);
					HttpEntity resEntityGet = responseGet.getEntity();
					if (resEntityGet != null) {
						String httpRet = EntityUtils.toString(resEntityGet, HTTP.UTF_8).trim();
						listenerHTTPCommunication.receiveResult(httpRet);
						
						LogUtil.d(LOG_TAG, "" + responseGet.getStatusLine().getStatusCode());
						if(responseGet.getStatusLine().getStatusCode() == 404){
							throw new Exception();
						}
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
					listenerHTTPCommunication.receiveResult("ERROR");
				} catch (IOException e) {
					e.printStackTrace();
					listenerHTTPCommunication.receiveResult("ERROR");
				} catch (Exception e) {
					e.printStackTrace();
					listenerHTTPCommunication.receiveResult("ERROR");
				}
			}
		}).start();
	}

	/**
	 * send and get result
	 * 
	 * @param uRLString
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public String getHTTPResult(final String uRLString) throws ClientProtocolException, IOException {
		String resultString = "";

		HttpClient client = new DefaultHttpClient();
		client.getParams().setParameter("http.protocol.expect-continue", false);
		client.getParams().setParameter("http.connection.timeout", TIME_OUT);
		client.getParams().setParameter("http.socket.timeout", TIME_OUT);
		// String getURL = "http://localhost";

		HttpGet get = new HttpGet(uRLString);
		HttpResponse responseGet;

		responseGet = client.execute(get);
		HttpEntity resEntityGet = responseGet.getEntity();
		if (resEntityGet != null) {
			resultString = EntityUtils.toString(resEntityGet, HTTP.UTF_8).trim();
			// LogUtil.d(LOG_TAG, resultString);
		}

		return resultString;
	}

	/**
	 * post array data and get result
	 * 
	 * @param urlString
	 * @throws Exception
	 */
	public String getHTTPPostResult(String urlString, HashMap<String, String> hashMap) throws Exception {
		HttpPost sendHttpPost = new HttpPost(urlString);
		Vector<BasicNameValuePair> entity = new Vector<BasicNameValuePair>();

		for (String keyString : hashMap.keySet()) {
			if (keyString.length() > 0) {
				entity.add(new BasicNameValuePair(keyString, hashMap.get(keyString)));
			}
		}

		sendHttpPost.setEntity(new UrlEncodedFormEntity(entity, "utf-8"));

		// https client
		HttpClient client = new DefaultHttpClient();

		client.getParams().setParameter("http.protocol.expect-continue", false);
		client.getParams().setParameter("http.connection.timeout", TIME_OUT);
		client.getParams().setParameter("http.socket.timeout", TIME_OUT);

		HttpResponse response = client.execute(sendHttpPost);
		String resultString = "";

		if (response.getStatusLine().getStatusCode() == 200) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(	response.getEntity()
																								.getContent(), "utf-8"));
			String lineString = null;
			while ((lineString = bufferedReader.readLine()) != null) {
				resultString += lineString;
			}
		}

		return resultString;
	}

	/**
	 * return is file exists
	 * 
	 * @param fileAddress
	 * @param localFileName
	 * @param downloadDir
	 * @return
	 */
	private static boolean fileUrlCacheCheck(String fileAddress, String localFileName, String downloadDir) {
		File file = new File(downloadDir + File.separator + localFileName);
		
		long lastModified = file.lastModified();
		String lastModifiedString = DateUtils.formatDate(new Date(lastModified), "EEE, d MMM yyyy HH:mm:ss 'GMT'");

		HttpClient client = new DefaultHttpClient();
		client.getParams().setParameter("http.protocol.expect-continue", false);
		client.getParams().setParameter("http.connection.timeout", TIME_OUT);
		client.getParams().setParameter("http.socket.timeout", TIME_OUT);
		// String getURL = "http://localhost";

		HttpResponse responseGet;
		try {
			// for special character
			URI uriFile = new URI(fileAddress);

			HttpGet get = new HttpGet(uriFile);
			get.addHeader("If-Modified-Since", lastModifiedString);

			responseGet = client.execute(get);
			HttpEntity resEntityGet = responseGet.getEntity();
			if (responseGet.getStatusLine().getStatusCode() > 300) {
				return true;
			} else {
				return false;
			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return false;
	}
	/**
	 * buffer size
	 */
	final static int size = 1024;
	/**
	 * file get
	 * 
	 * @param fileAddress
	 * @param localFileName
	 * @param downloadDir
	 * @throws IOException
	 */
	public static void fileUrlDownload(String fileAddress, String localFileName, String downloadDir) throws IOException {

		// cache enabled
		if (fileUrlCacheCheck(fileAddress, localFileName, downloadDir)) {
			System.out.println("-------File Exists Skipped : " + localFileName + "------");
			return;
		}

		OutputStream outStream = null;
		URLConnection uCon = null;
		InputStream is = null;
		// LogUtil.println("-------Download Start------");
		System.out.println(fileAddress);
		URL url;
		byte[] buf;
		int byteRead;
		int byteWritten = 0;
		url = new URL(fileAddress);
		outStream = new BufferedOutputStream(new FileOutputStream(downloadDir + File.separator + localFileName));
		uCon = url.openConnection();
		uCon.setConnectTimeout(TIME_OUT);
		uCon.setReadTimeout(TIME_OUT);

		is = uCon.getInputStream();
		buf = new byte[size];
		while ((byteRead = is.read(buf)) != -1) {
			outStream.write(buf, 0, byteRead);
			byteWritten += byteRead;
		}
		// LogUtil.println("Download Successfully.");
		// LogUtil.println("File name : " + localFileName);
		// LogUtil.println("of bytes  : " + byteWritten);
		// LogUtil.println("-------Download End--------");
		is.close();
		outStream.close();
	}
}
