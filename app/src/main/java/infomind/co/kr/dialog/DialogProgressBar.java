package infomind.co.kr.dialog;

import infomind.co.kr.kiosk.air.R;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;

public class DialogProgressBar extends Dialog {

	public DialogProgressBar(Context context) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
//		super(context, android.R.style.Theme_Translucent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

//		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
//		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
//		lpWindow.flags = WindowManager.LayoutParams.FLAG_FULLSCREEN;
//		lpWindow.dimAmount = 0.1f;
//		getWindow().setAttributes(lpWindow);

		setContentView(R.layout.dialog_progressbar);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_MENU) {
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

}
