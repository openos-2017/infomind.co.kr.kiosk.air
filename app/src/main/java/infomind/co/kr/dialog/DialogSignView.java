package infomind.co.kr.dialog;

import infomind.co.kr.kiosk.air.R;
import infomind.co.kr.kiosk.air.SignView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DialogSignView extends Dialog {

	/**
	 * 서명 뷰 
	 */
	private SignView signView;
	/**
	 * 결제 승인 번호
	 */
	private String approvalNo = "";
	/**
	 * 입력 요구 문구 (다국어를 위해) 
	 */
	private String requestMessage = "";
	/**
	 * 재입력 요구 문구 (다국어를 위해) 
	 */
	private String retryMessage = "";
	
	/**
	 * 메시지 출력 
	 */
	private TextView L_text;
	
	/**
	 * 확인 버튼 
	 */
	private Button confirm;
	

	public DialogSignView(Context context, String approvalNo, String requestMessage, String retryMessage ) throws JSONException {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);

		this.approvalNo = approvalNo;
		this.requestMessage = requestMessage;
		this.retryMessage = retryMessage;
		
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.dialog_signview);

		signView = (SignView) findViewById(R.id.signView);
		L_text = (TextView) findViewById(R.id.L_text);
		L_text.setText(requestMessage);
		
		confirm = (Button) findViewById(R.id.confirm);
		confirm.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 저장 후 종료한다. 
				dismiss();
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_MENU) {
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	public void dismiss() {
		// 서명을 저장한다.
		byte[] data = signView.get1BitBitmap();
		if (data.length > 0) {
			Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
			SaveBitmapToFileCache(bmp, Environment.getExternalStorageDirectory().getPath() 
			                      + "/SignImage_" + System.currentTimeMillis() + "_" + approvalNo + ".jpg");
			super.dismiss();
		}else{
			L_text.setText(retryMessage);
			return;
		}
	}

	/** bitmap을 저장한다. 
	 * @param bitmap
	 * @param strFilePath 파일명 포함 경로 
	 */
	private void SaveBitmapToFileCache(Bitmap bitmap, String strFilePath) {

		File fileCacheItem = new File(strFilePath);
		OutputStream out = null;

		try {
			fileCacheItem.createNewFile();
			out = new FileOutputStream(fileCacheItem);

			bitmap.compress(CompressFormat.JPEG, 100, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
