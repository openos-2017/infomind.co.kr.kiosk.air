package infomind.co.kr.wechat;

import infomind.co.kr.wechat.WeChatResultOptions;

import org.json.JSONException;

public interface OnWeChatResultReceived {
	void returnReceivedData(WeChatResultOptions resultOptions) throws JSONException;
	
	void networkTimeout(WeChatOptions options);
}
