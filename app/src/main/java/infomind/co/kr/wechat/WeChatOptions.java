package infomind.co.kr.wechat;

public class WeChatOptions {
	
	/* 거래구분 */
	private String paymentType = "BE"; 
	
	/* 전문번호  (유니크 값) */ 
	private String uniqueNo = "";

	/* 합계금액 */
	private String paymentAmount = "";
	
	/* 승인번호 12자리 */
	private String approvalNo = "            ";
	
	/* 승인일자 */
	private String approvalDate = "      ";
	
	/* 상품명 */
	private String productName = "";
	
	/* 바코드번호 */
	private String barcodeNo = "";
	
	/* 조회구분 2자리*/
	private String informationType = "  "; 
	
	
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	
	public String getApprovalNo() {
		return approvalNo;
	}
	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	public String getUniqueNo() {
		return uniqueNo;
	}
	public void setUniqueNo(String uniqueNo) {
		this.uniqueNo = uniqueNo;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getBarcodeNo() {
		return barcodeNo;
	}
	public void setBarcodeNo(String barcodeNo) {
		this.barcodeNo = barcodeNo;
	}
	public String getInformationType() {
		return informationType;
	}
	public void setInformationType(String informationType) {
		this.informationType = informationType;
	}
	public String getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}
	
	public String toJSONString() {
		return "{ \"paymentType\" :  \"" + paymentType + "\", \"uniqueNo\" :  \"" + uniqueNo + "\", \"paymentAmount\" :  \"" + paymentAmount
				+ "\", \"approvalNo\" :  \"" + approvalNo + "\", \"approvalDate\" :  \"" + approvalDate + "\", \"productName\" :  \"" + productName + "\", \"barcodeNo\" :  \"" + barcodeNo
				+ "\", \"informationType\" :  \"" + informationType + "\"}";
	}
}
