package infomind.co.kr.wechat;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

public class WeChatPayment {
	
	private String SERVER_IP = "210.181.28.137"; // "210.181.28.116"; // 210.181.28.137
	private String M_ID = "DPT0F50004"; // "DPT0TEST03"; // DPT0F50000
	/**
	 * 결과 수신용 
	 */
	private OnWeChatResultReceived onResultReceived; 
	public OnWeChatResultReceived getOnResultReceived() {
		return onResultReceived;
	}
	public void setOnResultReceived(OnWeChatResultReceived onResultReceived) {
		this.onResultReceived = onResultReceived;
	}

	/**
	 * 통신 스레드 
	 */
	private Thread communicationThread;
	
	/**
	 * 타임 아웃 
	 */
	private static final int NETWORK_TIMEOUT = 60000;
	/**
	 * 타임아웃 타이머 태스크 
	 */
	private TimerTask timeoutTask;
	public Thread getCommunicationThread() {
		return communicationThread;
	}
	public void setCommunicationThread(Thread communicationThread) {
		this.communicationThread = communicationThread;
	}
	/**
	 * uniqueNo size 12
	 * barcodeNo size < 128
	 * productName size < 32
	 * paymentAmount size < 9
	 * paymentType size 2 default "BE"
	 * informationType size 2 default "  " 
	 * approvalNo size 12 default "            "
	 * approvalDate size 6 default "      "
	 */
	public void payment(final WeChatOptions options){
		communicationThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				com.ksnet.interfaces.Approval approval = new com.ksnet.interfaces.Approval();
				
				java.nio.ByteBuffer bb = java.nio.ByteBuffer.allocateDirect(4096);
				
				////////////////////////////////////////////////////////////////////////////////////////////////////
				// 신규 암호화 통합 전문
				////////////////////////////////////////////////////////////////////////////////////////////////////
				
				bb.put((byte)0x02);                                             // 1.STX
				bb.put(options.getPaymentType().getBytes());                                        // 2.거래구분
				bb.put(M_ID.getBytes());                                // 3.단말기번호
				
				for(int i=0; i< 4; i++) bb.put(" ".getBytes());                 // 4.업체정보
				
				//for(int i=0; i<12; i++) bb.put(" ".getBytes());                 // 5.전문일련번호
				bb.put(options.getUniqueNo().getBytes());
				
				bb.put("K".getBytes());                                         // 6.POS Entry Mode
				bb.put("****************=********************".getBytes());     // 7.Track II
				bb.put((byte)0x1C);                                             // 8.FS
				
//				for(int i=0; i< 2; i++) bb.put(" ".getBytes());					// 9.조회구분
				bb.put(options.getInformationType().getBytes());
				
				// 자릿수 맞춤 
				String tPaymentAmount = options.getPaymentAmount();
				for(int i = 0; i < 9 - options.getPaymentAmount().length(); i++){
					tPaymentAmount = "0" + tPaymentAmount;
				}
				bb.put(tPaymentAmount.getBytes());                              	// 10.총금액
				bb.put("000000000".getBytes());                              	// 11.부분취소금액
				bb.put("000000000".getBytes());                              	// 12.세금
				bb.put("000000000".getBytes());                              	// 13.공급금액
				
				bb.put("AA".getBytes());                                        // 14.Working Key Index
				for(int i=0; i<16; i++) bb.put("0".getBytes());                 // 15.비밀번호
				
				bb.put(options.getApprovalNo().getBytes());                     // 16.원거래승인번호
				bb.put(options.getApprovalDate().getBytes());                   // 17.원거래승인일자
				for(int i=0; i<13; i++) bb.put(" ".getBytes());                 // 18.사용자정보 ~ DCC 환율조회 Data
				
				for(int i=0; i<2; i++) bb.put(" ".getBytes());					// 19.가맹점 ID 
				for(int i=0; i<30; i++) bb.put(" ".getBytes());					// 20.가맹점 사용필드 
				
				bb.put("WXKR".getBytes());										// 21.포인트 구분 
				for(int i=0; i<20; i++) bb.put(" ".getBytes());					// 22.KSNET Reserved 
				
				bb.put("N".getBytes()); 										// 23.동글구분
				bb.put("S".getBytes()); 										// 24.매체구분 
				bb.put(" ".getBytes()); 										// 25.이통사구분 
				bb.put(" ".getBytes()); 										// 26.신용카드종류 
				bb.put("N".getBytes()); 										// 27.거래형태 
				
				bb.put("N".getBytes());                                         // 28.전자서명 유무
				
				// 바코드 
				String tempPaymentNumber = options.getBarcodeNo(); //"130529372143472171"; 
				for(int i = 0; i < 128 - options.getBarcodeNo().length(); i++){
					tempPaymentNumber = tempPaymentNumber + " ";
				}
				//tempPaymentNumber += "+";
				
				// 상품명
				String tempProductName = "Test Product";//options.getProductName(); 
				for(int i = 0; i < 72 - "Test Product".length(); i++){
					tempProductName = tempProductName + " ";
				}
				
				bb.put(tempPaymentNumber.getBytes());							// 29.위챗페이정보
				bb.put(tempProductName.getBytes());								// 29.상품명
				
				bb.put((byte)0x03);                                             // 30.ETX
				bb.put((byte)0x0D);                                             // 31.CR
				
				byte[] telegram = new byte[ bb.position() ];
				bb.rewind();
				bb.get( telegram );		
				
				byte[] requestTelegram = new byte[telegram.length + 4];
				String telegramLength = String.format("%04d", telegram.length);
				System.arraycopy(telegramLength.getBytes(), 0, requestTelegram, 0, 4              );
				System.arraycopy(telegram                 , 0, requestTelegram, 4, telegram.length);
				
				System.out.println("requestTelegram length : " + requestTelegram.length); // 318? 
				System.out.println("requestTelegram: [" + new String(requestTelegram, 0, requestTelegram.length) + "]");
				
				byte[] responseTelegram = new byte[2048];
				int rtn = approval.request(SERVER_IP, 9562, 5, requestTelegram, responseTelegram, 10000);
				
				try {
					Thread.sleep(100);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					return;
				}

				// 정상 수신시 타이머 태스크 종료 
				timeoutTask.cancel();
				
				System.out.println("rtn: [" + rtn + "]");
				
				WeChatResultOptions resultOptions = new WeChatResultOptions();
				
				// 0: 정상, -9: 망취소 상황이 발생하였으나 망취소 대상이 아님, -100: 망취소
				if(rtn > 0 || rtn == -9 || rtn == -100) {
					// 필요하다면 뒷부분을 자른다
					int i = 0;
					for(i=responseTelegram.length-1; i>=0; i--) if(responseTelegram[i] != (byte)0x00) break;				
					byte[] tmp = new byte[i];
					System.arraycopy(responseTelegram, 0, tmp, 0, tmp.length);
					System.out.println("responseTelegram: [" + new String(tmp, 0, tmp.length) + "]");
					
					int index = 0;
					resultOptions.setTotalLength(new String(tmp, index, 4)); index += 4;					// 길이
					resultOptions.setStx(""); index += 1;												// STX (특수문자) 
					resultOptions.setPaymentType(new String(tmp, index, 2)); index += 2;					// 거래구분 
					resultOptions.setTerminolNo(new String(tmp, index, 10)); index += 10;				// 단말기번호 
					resultOptions.setApprovalShop(new String(tmp, index, 4)); index += 4;					// 업체정보 
					resultOptions.setUniqueNo(new String(tmp, index, 12)); index += 12;				// 전문번호 (Unique)
					resultOptions.setStatus(new String(tmp, index, 1)); index += 1;					// Status
					resultOptions.setApprovalDateTime(new String(tmp, index, 12)); index += 12;					// 거래일시  
					resultOptions.setCardType(new String(tmp, index, 1)); index += 1;					// 카드 Type
					resultOptions.setMessage1(new String(tmp, index, 16)); index += 16;					// Message1
					resultOptions.setMessage2(new String(tmp, index, 16)); index += 16;					// Message2
					resultOptions.setApprovalNo(new String(tmp, index, 12)); index += 12;					// 승인번호 
					resultOptions.setPointShopNo(new String(tmp, index, 15)); index += 15;					// 가맹점번호 
					resultOptions.setPointIssueBinCode(new String(tmp, index, 2)); index += 2;					// 발급사코드 
					resultOptions.setIssuebinName(new String(tmp, index, 16)); index += 16;					// 카드종류명 
					resultOptions.setPurchaseCode(new String(tmp, index, 2)); index += 2;					// 매입사코드 
					resultOptions.setPurchaseName(new String(tmp, index, 16)); index += 16;					// 매입사명 
					resultOptions.setWorkingKeyIndex(new String(tmp, index, 2)); index += 2;					// Working Key index
					resultOptions.setWorkingKey(new String(tmp, index, 16)); index += 16;					// Working Key 
					resultOptions.setPointUsePossible(new String(tmp, index, 9)); index += 9;					// 예비포인트 
					resultOptions.setPaymentAmount(new String(tmp, index, 9)); index += 9;					// 포인트 1 (CNY 고객 금액 9,2) -- KRW 
					resultOptions.setPaymentPoint2(new String(tmp, index, 9)); index += 9;					// 포인트 2 (결제금액 9,2) -- 중국 위완화 
					resultOptions.setPaymentPoint3(new String(tmp, index, 9)); index += 9;					// 포인트 3 고객 청구 통화 
					resultOptions.setNotice1(new String(tmp, index, 20)); index += 20;				// Notice 1 
					resultOptions.setNotice2(new String(tmp, index, 40)); index += 40;					// Notice2 
					resultOptions.setPayment(new String(tmp, index, 1)); index += 1;					// 거래형태 
					resultOptions.setReserved(new String(tmp, index, 5)); index += 5;					// Reserved 
					resultOptions.setKsnetReserved(new String(tmp, index, 40)); index += 40;					// KSNET Reserved 
					resultOptions.setEtx(""); index += 1;												// ETX
//					resultOptions.setCr(new String(tmp, index, 1)); index += 1;					// CR (앞에서 하나 잘랐슴)  
					
				}	
				
				try {
					onResultReceived.returnReceivedData(resultOptions);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		// NETWORK_TIMEOUT 이후 에러 처리 
		timeoutTask = new TimerTask() {
			
			@Override
			public void run() {
				// interrupt thread : onResultReceived.returnReceivedData 결과 반환이 없다. 
				communicationThread.interrupt();
				// call back : 호출 정보를 넘김 
				onResultReceived.networkTimeout(options);
			}
		};
		new Timer().schedule(timeoutTask, NETWORK_TIMEOUT);
		
		// 송신 시작
		communicationThread.start();
		
	}
	
}
