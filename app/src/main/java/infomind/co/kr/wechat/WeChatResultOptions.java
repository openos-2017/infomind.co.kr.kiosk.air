package infomind.co.kr.wechat;

public class WeChatResultOptions {
	private String totalLength;					// 길이
	private String stx;							// stx 
	private String paymentType;					// 거래구분 
	private String terminolNo;					// 단말기번호 
	private String approvalShop;				// 업체정보 
	private String uniqueNo;					// 전문번호 	
	private String status;						// Status 
	private String approvalDateTime;			// 거래일시 
	private String cardType;					// 카드 Type 
	private String message1;					// Message1 
	private String message2;					// Message2 
	private String approvalNo;					// 승인번호 
	private String pointShopNo;					// 가맹점번호 
	private String pointIssueBinCode; 			// 발급사코드
	private String issuebinName; 				//	카드종류명
	private String purchaseCode; 				// 매입사코드
	private String purchaseName; 				// 매입사명
	private String workingKeyIndex; 			// Working Key Index
	private String workingKey; 					// Working Key   
	private String pointUsePossible; 			// 예비 포인트
	private String paymentAmount; 				// 포인트 1
	private String paymentPoint2;				// 포인트 2
	private String paymentPoint3;				// 포인트 3
	private String notice1;						// notice1 						
	private String notice2;						// notice2 
	private String payment;						// 거래형태 
	private String reserved; 					// Reserved
	private String ksnetReserved;				// KSNET Reserved
	private String etx; 						// ETX
	private String cr;							// CR
	
	public String getTotalLength() {
		return totalLength;
	}
	public void setTotalLength(String totalLength) {
		this.totalLength = totalLength;
	}
	public String getStx() {
		return stx;
	}
	public void setStx(String stx) {
		this.stx = stx;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getTerminolNo() {
		return terminolNo;
	}
	public void setTerminolNo(String terminolNo) {
		this.terminolNo = terminolNo;
	}
	public String getApprovalShop() {
		return approvalShop;
	}
	public void setApprovalShop(String approvalShop) {
		this.approvalShop = approvalShop;
	}
	public String getUniqueNo() {
		return uniqueNo;
	}
	public void setUniqueNo(String uniqueNo) {
		this.uniqueNo = uniqueNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getApprovalDateTime() {
		return approvalDateTime;
	}
	public void setApprovalDateTime(String approvalDateTime) {
		this.approvalDateTime = approvalDateTime;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getMessage1() {
		return message1;
	}
	public void setMessage1(String message1) {
		this.message1 = message1;
	}
	public String getMessage2() {
		return message2;
	}
	public void setMessage2(String message2) {
		this.message2 = message2;
	}
	public String getApprovalNo() {
		return approvalNo;
	}
	public void setApprovalNo(String approvalNo) {
		this.approvalNo = approvalNo;
	}
	public String getPointShopNo() {
		return pointShopNo;
	}
	public void setPointShopNo(String pointShopNo) {
		this.pointShopNo = pointShopNo;
	}
	public String getPointIssueBinCode() {
		return pointIssueBinCode;
	}
	public void setPointIssueBinCode(String pointIssueBinCode) {
		this.pointIssueBinCode = pointIssueBinCode;
	}
	public String getIssuebinName() {
		return issuebinName;
	}
	public void setIssuebinName(String issuebinName) {
		this.issuebinName = issuebinName;
	}
	public String getPurchaseCode() {
		return purchaseCode;
	}
	public void setPurchaseCode(String purchaseCode) {
		this.purchaseCode = purchaseCode;
	}
	public String getPurchaseName() {
		return purchaseName;
	}
	public void setPurchaseName(String purchaseName) {
		this.purchaseName = purchaseName;
	}
	public String getWorkingKeyIndex() {
		return workingKeyIndex;
	}
	public void setWorkingKeyIndex(String workingKeyIndex) {
		this.workingKeyIndex = workingKeyIndex;
	}
	public String getWorkingKey() {
		return workingKey;
	}
	public void setWorkingKey(String workingKey) {
		this.workingKey = workingKey;
	}
	public String getPointUsePossible() {
		return pointUsePossible;
	}
	public void setPointUsePossible(String pointUsePossible) {
		this.pointUsePossible = pointUsePossible;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getPaymentPoint2() {
		return paymentPoint2;
	}
	public void setPaymentPoint2(String paymentPoint2) {
		this.paymentPoint2 = paymentPoint2;
	}
	public String getPaymentPoint3() {
		return paymentPoint3;
	}
	public void setPaymentPoint3(String paymentPoint3) {
		this.paymentPoint3 = paymentPoint3;
	}
	public String getNotice1() {
		return notice1;
	}
	public void setNotice1(String notice1) {
		this.notice1 = notice1;
	}
	public String getNotice2() {
		return notice2;
	}
	public void setNotice2(String notice2) {
		this.notice2 = notice2;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getReserved() {
		return reserved;
	}
	public void setReserved(String reserved) {
		this.reserved = reserved;
	}
	public String getKsnetReserved() {
		return ksnetReserved;
	}
	public void setKsnetReserved(String ksnetReserved) {
		this.ksnetReserved = ksnetReserved;
	}
	public String getEtx() {
		return etx;
	}
	public void setEtx(String etx) {
		this.etx = etx;
	}
	public String getCr() {
		return cr;
	}
	public void setCr(String cr) {
		this.cr = cr;
	}
	
	public String toString() {
		return "ResultOptions [totalLength=" + totalLength + ", stx=" + stx + ", paymentType=" + paymentType
				+ ", terminolNo=" + terminolNo + ", approvalShop=" + approvalShop + ", uniqueNo=" + uniqueNo
				+ ", status=" + status + ", approvalDateTime=" + approvalDateTime + ", cardType=" + cardType
				+ ", message1=" + message1 + ", message2=" + message2 + ", approvalNo=" + approvalNo + ", pointShopNo="
				+ pointShopNo + ", pointIssueBinCode=" + pointIssueBinCode + ", issuebinName=" + issuebinName
				+ ", purchaseCode=" + purchaseCode + ", purchaseName=" + purchaseName + ", workingKeyIndex="
				+ workingKeyIndex + ", workingKey=" + workingKey + ", pointUsePossible=" + pointUsePossible
				+ ", paymentAmount=" + paymentAmount + ", paymentPoint2=" + paymentPoint2 + ", paymentPoint3="
				+ paymentPoint3 + ", notice1=" + notice1 + ", notice2=" + notice2 + ", payment=" + payment
				+ ", reserved=" + reserved + ", ksnetReserved=" + ksnetReserved + ", etx=" + etx + ", cr=" + cr + "]";
	}
	
	public String toJSONString() {
		return  "{\"totalLength\" : \"" + totalLength + "\", \"stx\" : \"" + stx + "\", \"paymentType\" : \"" + paymentType
				+ "\", \"terminolNo\" : \"" + terminolNo + "\", \"approvalShop\" : \"" + approvalShop + "\", \"uniqueNo\" : \"" + uniqueNo
				+ "\", \"status\" : \"" + status + "\", \"approvalDateTime\" : \"" + approvalDateTime + "\", \"cardType\" : \"" + cardType
				+ "\", \"message1\" : \"" + message1 + "\", \"message2\" : \"" + message2 + "\", \"approvalNo\" : \"" + approvalNo + "\", \"pointShopNo\" : \""
				+ pointShopNo + "\", \"pointIssueBinCode\" : \"" + pointIssueBinCode + "\", \"issuebinName\" : \"" + issuebinName
				+ "\", \"purchaseCode\" : \"" + purchaseCode + "\", \"purchaseName\" : \"" + purchaseName + "\", \"workingKeyIndex\" : \""
				+ workingKeyIndex + "\", \"workingKey\" : \"" + workingKey + "\", \"pointUsePossible\" : \"" + pointUsePossible
				+ "\", \"paymentAmount\" : \"" + paymentAmount + "\", \"paymentPoint2\" : \"" + paymentPoint2 + "\", \"paymentPoint3\" : \""
				+ paymentPoint3 + "\", \"notice1\" : \"" + notice1 + "\", \"notice2\" : \"" + notice2 + "\", \"payment\" : \"" + payment
				+ "\", \"reserved\" : \"" + reserved + "\", \"ksnetReserved\" : \"" + ksnetReserved + "\", \"etx\" : \"" + etx + "\", \"cr\" : \"" + cr + "\"}";
	}
	
}
